# Disco's Lab

This is a place for me to share (sometimes unfinished) projects. 

## Quick start

If all you want to do is run this code with minimal setup and don't intend to hack on it,
then you can get everything up and running with `docker-compose`.

```bash
docker-compose --profile=all up -d
```

This will expose the following:
  - [http://localhost:1101/](http://localhost:1101/) - Web frontend for [mailhog](https://github.com/mailhog/MailHog) which shows sent emails
  - [http://localhost:3000/docs](http://localhost:3000/docs) - Swagger page for the [webapi](./webapi/README.md)
  - [http://localhost:4000/](http://localhost:4000/) - The web [frontend](./frontend/README.md)

## Configuration

Everything is configured in this repo with environment variables.

Please see [.envrc.sample](./.envrc.sample) for a good starting point.

You can manage these for development with [direnv](https://direnv.net/):

```bash
echo "source_env_if_exists .envrc.sample" > .envrc
direnv allow
```

## Contents

  - [frontend](./frontend/README.md): Frontend for the website
  - [images](./images/README.md): Custom docker images
  - [infra](./infra/README.md): Provision and configure infrastructure 
  - [lib](./lib/README.md): Shared libraries
  - [webapi](./webapi/README.md): REST API for the website
