# Disco's Lab Infrastructure

Provision resources in the cloud using the `terraform` script in the [provision](./provision/README.md) folder.

Configure and deploy software to the infrastructure with the `ansible` playbook in the [configure](./configure/README.md) folder.
