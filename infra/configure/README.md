# Disco's Lab - Ansible Playbook

Playbook for configuring infrastructure at Disco's Lab, as well as deploying software to production.


## Getting started

Setup environment variables with `direnv`. See [.envrc.sample](../../.envrc.sample)

Configure all of the infrastructure and deploy Disco's Lab software to it:
```
ansible-playbook -i inventories/production.yml -K main.yml 
```
