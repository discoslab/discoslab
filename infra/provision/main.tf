module "webserver" {
  source = "./modules/webserver"

  name   = "webserver"
  image  = "debian-11-x64"
  region = "nyc3"
  size   = "s-1vcpu-1gb"

  domain = var.domain

  tags = [
    "discoslab",
    "terraform",
    "webserver",
  ]

  ssh_port = var.ssh_port
  ssh_keys = var.ssh_keys

  ansible_user_name = var.ansible_user_name
  ansible_user_password = var.ansible_user_password
}

module "fastmail" {
  source = "./modules/fastmail"

  depends_on = [
    module.webserver,
  ]

  domain = var.domain
}
