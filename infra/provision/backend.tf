terraform {
  backend "s3" {
    key                         = "discoslab/terraform.tfstate"
    bucket                      = "discoslab-tf-state"
    region                      = "us-nyc-3"
    endpoint                    = "https://nyc3.digitaloceanspaces.com"
    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
  }
}

