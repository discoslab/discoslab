# Terraform - Infrastructure Creation
Collection of terraform files for creating all of the infrastructure at Disco's lab.

# Getting Started
- Ensure all environment variables are set. See [.envrc.sample](../../.envrc.sample)
- Run `terraform init` to initialize the terraform project.
- You can then run `terraform plan` or `terraform apply` to change the infrastructure.

# Tips
- `doctl compute ssh-key list` to see the ssh key ids from digital ocean
