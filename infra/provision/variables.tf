variable "digitalocean_token" {
  type        = string
  description = "Access token for Digital Ocean. See https://docs.digitalocean.com/reference/api/create-personal-access-token/"
  sensitive   = true
}

variable "ssh_keys" {
  type        = list(number)
  description = "List of ssh keys that get added to droplets when creating them. Use the IDs from `doctl compute ssh-key list`."
  sensitive   = true
}

variable "ssh_port" {
  type        = number
  description = "Port number to allow ssh connections on."
  sensitive   = true
}

variable "ansible_user_name" {
  type        = string
  description = "The password for the ansible user."
  sensitive   = true
}

variable "ansible_user_password" {
  type        = string
  description = "The password for the ansible user."
  sensitive   = true
}

variable "domain" {
  type        = string
  description = "Domain of the application."
  default     = "discoslab.com"
}
