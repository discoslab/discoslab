resource "digitalocean_record" "mx-1" {
  domain   = var.domain
  type     = "MX"
  name     = "@"
  priority = 10
  value    = "in1-smtp.messagingengine.com."
}

resource "digitalocean_record" "mx-2" {
  domain   = var.domain
  type     = "MX"
  name     = "@"
  priority = 20
  value    = "in2-smtp.messagingengine.com."
}

resource "digitalocean_record" "dkim-1" {
  domain = var.domain
  type   = "CNAME"
  name   = "fm1._domainkey"
  value  = "fm1.discoslab.com.dkim.fmhosted.com."
}

resource "digitalocean_record" "dkim-2" {
  domain = var.domain
  type   = "CNAME"
  name   = "fm2._domainkey"
  value  = "fm2.discoslab.com.dkim.fmhosted.com."
}

resource "digitalocean_record" "dkim-3" {
  domain = var.domain
  type   = "CNAME"
  name   = "fm3._domainkey"
  value  = "fm3.discoslab.com.dkim.fmhosted.com."
}

resource "digitalocean_record" "spf" {
  domain = var.domain
  type   = "TXT"
  name   = "@"
  value  = "v=spf1 include:spf.messagingengine.com ?all"
}
