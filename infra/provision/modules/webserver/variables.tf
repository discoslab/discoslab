variable "name" {
  type        = string
  description = "Name of the webserver to create in Digital Ocean."
}

variable "image" {
  type        = string
  description = "The Droplet image ID or slug used for the webserver. Use the Slugs from `doctl compute image list --public`."
}

variable "domain" {
  type        = string
  description = "Domain name of the webserver."
}

variable "ssh_port" {
  type        = number
  description = "Port number to allow ssh connections on."
  sensitive   = true
}

variable "ssh_keys" {
  type        = list(number)
  description = "List of ssh keys that get added to the servers when creating them. Use the IDs from `doctl compute ssh-key list`."
  sensitive   = true
}

variable "ansible_user_name" {
  type        = string
  description = "The password for the ansible user."
  sensitive   = true
}

variable "ansible_user_password" {
  type        = string
  description = "The password for the ansible user, which will be used later to configure the server."
  sensitive   = true
}

variable "tags" {
  type        = list(string)
  description = "List of tags to associate to the resources."
  default     = []
}

variable "region" {
  type        = string
  description = "The region where the webserver will be created. Use the Slugs from `doctl compute region list`."
}

variable "size" {
  type        = string
  description = "Size of the Droplet used for the webserver. Use the Slugs from `doctl compute size list`."
  default     = "s-1vcpu-1gb"
}
