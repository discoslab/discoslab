locals {
  user_data_vars = tomap({
    ssh_port              = var.ssh_port
    ansible_user_name     = var.ansible_user_name
    ansible_user_password = var.ansible_user_password
  })
}

resource "digitalocean_droplet" "webserver" {
  image     = var.image
  name      = var.name
  region    = var.region
  size      = var.size
  ssh_keys  = var.ssh_keys
  tags      = var.tags
  ipv6      = true
  user_data = templatefile("./templates/user-data.sh.tftpl", local.user_data_vars)
}

resource "digitalocean_domain" "webserver" {
  name       = var.domain
  ip_address = digitalocean_droplet.webserver.ipv4_address
}

resource "digitalocean_record" "webserver-aaaa" {
  domain = digitalocean_domain.webserver.id
  type   = "AAAA"
  name   = "@"
  value  = digitalocean_droplet.webserver.ipv6_address
}

resource "digitalocean_record" "webserver-www-a" {
  domain = digitalocean_domain.webserver.id
  type   = "A"
  name   = "www"
  value  = digitalocean_droplet.webserver.ipv4_address
}

resource "digitalocean_record" "webserver-www-aaaa" {
  domain = digitalocean_domain.webserver.id
  type   = "AAAA"
  name   = "www"
  value  = digitalocean_droplet.webserver.ipv6_address
}
