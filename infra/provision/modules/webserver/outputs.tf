output "id" {
  value       = digitalocean_droplet.webserver.id
  description = "ID of the webserver's droplet."
}

output "ipv4_address" {
  value       = digitalocean_droplet.webserver.ipv4_address
  description = "IPv4 address to the webserver's droplet."
}

output "ipv6_address" {
  value       = digitalocean_droplet.webserver.ipv6_address
  description = "IPv6 address to the webserver's droplet."
}

output "price_hourly" {
  value       = digitalocean_droplet.webserver.price_hourly
  description = "Droplet hourly price"
}

output "price_monthly" {
  value       = digitalocean_droplet.webserver.price_monthly
  description = "Droplet monthly price"
}
