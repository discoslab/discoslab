module Discoslab.Webapi.Email.FromSpec
  ( FakeEmailFrom (..)
  , spec
  ) where

import Discoslab.Webapi.Email.Address qualified as EmailAddress
import Discoslab.Webapi.Email.AddressSpec (FakeEmailAddress)
import Discoslab.Webapi.Email.AddressSpec qualified as FakeEmailAddress
import Discoslab.Webapi.Email.From (EmailFrom (EmailFrom))
import Discoslab.Webapi.Email.From qualified as EmailFrom
import Discoslab.Webapi.Email.Name qualified as EmailName
import Discoslab.Webapi.Email.NameSpec (FakeEmailName)
import Discoslab.Webapi.Email.NameSpec qualified as FakeEmailName

import Data.Coerce (coerce)
import Network.Mail.Mime qualified as Mime
import Test.Hspec (Spec, shouldBe)
import Test.Hspec.QuickCheck (prop)
import Test.QuickCheck qualified as QuickCheck


newtype FakeEmailFrom = FakeEmailFrom EmailFrom
  deriving (Show, Eq)


instance QuickCheck.Arbitrary FakeEmailFrom where
  arbitrary = do
    name <- coerce $ QuickCheck.arbitrary @(Maybe FakeEmailName)
    address <- coerce $ QuickCheck.arbitrary @FakeEmailAddress
    pure . FakeEmailFrom $ EmailFrom{..}


  shrink (FakeEmailFrom emailFrom) = do
    name <- coerce . QuickCheck.shrink @(Maybe FakeEmailName) $ coerce emailFrom.name
    address <- coerce . QuickCheck.shrink @FakeEmailAddress $ coerce emailFrom.address
    pure . FakeEmailFrom $ EmailFrom{..}


spec :: Spec
spec =
  prop "can be converted to a mime address" $ \(FakeEmailFrom from) ->
    let actual = EmailFrom.toMimeAddress from
        expected =
          Mime.Address
            { Mime.addressName = fmap EmailName.toText from.name
            , Mime.addressEmail = EmailAddress.toText from.address
            }
     in actual `shouldBe` expected
