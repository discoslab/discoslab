module Discoslab.Webapi.Email.AddressSpec
  ( FakeEmailAddress (..)
  , spec
  ) where

import Discoslab.Webapi.Email.Address (EmailAddress)
import Discoslab.Webapi.Email.Address qualified as EmailAddress

import Control.Monad (replicateM)
import Data.Foldable (for_)
import Data.Maybe (mapMaybe)
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.Hspec.QuickCheck (prop)
import Test.QuickCheck qualified as QuickCheck


newtype ValidInput = ValidInput Text
  deriving (Show, Eq)


instance QuickCheck.Arbitrary ValidInput where
  arbitrary = do
    let genChar =
          QuickCheck.elements . mconcat $
            [ ['a' .. 'z']
            , ['A' .. 'Z']
            , ['0' .. '9']
            ]

        genText = do
          n <- QuickCheck.elements [3 .. 20]
          Text.pack <$> replicateM n genChar

    beforeAt <- do
      tld <-
        QuickCheck.oneof @Text
          [ pure "com"
          , pure "net"
          , pure "org"
          , pure "dev"
          , pure "xyz"
          , pure "co"
          , pure "edu"
          , pure "gov"
          , pure "mil"
          , pure "link"
          ]

      n <- QuickCheck.elements [1 .. 2]

      names <- replicateM n genText

      pure . mconcat $
        [ Text.intercalate "." names
        , "."
        , tld
        ]

    afterAt <- do
      n <- QuickCheck.elements [1 .. 2]
      names <- replicateM n genText
      pure $ Text.intercalate "." names

    pure . ValidInput $ beforeAt <> "@" <> afterAt


newtype FakeEmailAddress = FakeEmailAddress {value :: EmailAddress}
  deriving (Show, Eq)


instance QuickCheck.Arbitrary FakeEmailAddress where
  arbitrary = do
    QuickCheck.suchThatMap QuickCheck.arbitrary $ \(ValidInput text) ->
      case EmailAddress.fromText text of
        Right value -> Just $ FakeEmailAddress value
        _ -> Nothing


  shrink (FakeEmailAddress fakeEmail) =
    let shrunkenInputs = QuickCheck.shrink . ValidInput $ EmailAddress.toText fakeEmail
     in flip mapMaybe shrunkenInputs $ \(ValidInput text) ->
          case EmailAddress.fromText text of
            Right value -> Just $ FakeEmailAddress value
            _ -> Nothing


spec :: Spec
spec = do
  prop "works for any email" $ \(ValidInput input) ->
    let result = EmailAddress.toText <$> EmailAddress.fromText input
        expected = Right input
     in result `shouldBe` expected

  prop "works for any email with leading or trailing whitespace" $
    \(ValidInput input, QuickCheck.NonNegative leading, QuickCheck.NonNegative trailing) ->
      let
        result =
          fmap EmailAddress.toText . EmailAddress.fromText . mconcat $
            [ Text.replicate leading " "
            , input
            , Text.replicate trailing " "
            ]
        expected = Right input
       in
        result `shouldBe` expected

  describe "rejects empty strings" $
    let examples =
          [ ""
          , "\t"
          , "\n"
          , " "
          , " \t\n "
          ]
     in for_ examples $ \input ->
          it (show input) $
            EmailAddress.fromText input `shouldBe` Left EmailAddress.Invalid

  describe "rejects other invalid emails" $
    let examples =
          [ "kyle"
          , "stan"
          , "eric"
          , "kenny"
          , "timmy @ aol dot calm"
          ]
     in for_ examples $ \input ->
          it (show input) $
            EmailAddress.fromText input `shouldBe` Left EmailAddress.Invalid
