module Discoslab.Webapi.Email.NameSpec
  ( FakeEmailName (..)
  , spec
  ) where

import Discoslab.Webapi.Email.Name (EmailName)
import Discoslab.Webapi.Email.Name qualified as EmailName

import Control.Monad (replicateM)
import Data.Foldable (for_)
import Data.Maybe (mapMaybe)
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.Hspec.QuickCheck (prop)
import Test.QuickCheck qualified as QuickCheck


newtype ValidInput = ValidInput Text
  deriving (Show, Eq)


instance QuickCheck.Arbitrary ValidInput where
  arbitrary = do
    size <- do
      actualSize <- QuickCheck.getSize
      pure $
        if actualSize <= 0
          then 1
          else actualSize

    nameLengths <- do
      let genLength remainingSize lengths
            | remainingSize <= 0 = pure lengths
            | otherwise = do
                let isFirstWord = null lengths

                    upper
                      | isFirstWord = remainingSize
                      | remainingSize <= 1 = 1
                      | otherwise = remainingSize - 1

                    decreaseRemaining n
                      | isFirstWord = remainingSize - n
                      | otherwise = remainingSize - n - 1

                nameLength <- QuickCheck.chooseInt (1, upper)

                genLength (decreaseRemaining nameLength) (nameLength : lengths)

      genLength size []

    names <- do
      let genChar =
            QuickCheck.elements . mconcat $
              [ ['a' .. 'z']
              , ['A' .. 'Z']
              ]

          genName n =
            Text.pack <$> replicateM n genChar

      traverse genName nameLengths

    pure . ValidInput $ Text.unwords names


newtype FakeEmailName = FakeEmailName {value :: EmailName}
  deriving (Show, Eq)


instance QuickCheck.Arbitrary FakeEmailName where
  arbitrary = do
    QuickCheck.suchThatMap QuickCheck.arbitrary $ \(ValidInput text) ->
      case EmailName.fromText text of
        Right value -> Just $ FakeEmailName value
        _ -> Nothing


  shrink (FakeEmailName fakeEmail) =
    let shrunkenInputs = QuickCheck.shrink . ValidInput $ EmailName.toText fakeEmail
     in flip mapMaybe shrunkenInputs $ \(ValidInput text) ->
          case EmailName.fromText text of
            Right value -> Just $ FakeEmailName value
            _ -> Nothing


spec :: Spec
spec = do
  prop "accepts all valid input" $ \(ValidInput input) ->
    let result = EmailName.toText <$> EmailName.fromText input
        expected = Right input
     in result `shouldBe` expected

  prop "accepts all valid input with trailing or leading whitespace" $
    \(ValidInput input, QuickCheck.NonNegative leading, QuickCheck.NonNegative trailing) ->
      let
        result =
          fmap EmailName.toText . EmailName.fromText . mconcat $
            [ Text.replicate leading " "
            , input
            , Text.replicate trailing " "
            ]
        expected = Right input
       in
        result `shouldBe` expected

  describe "rejects empty strings" $
    let examples =
          [ ""
          , "\t"
          , "\n"
          , " "
          , " \t\n "
          ]
     in for_ examples $ \input ->
          it (show input) $
            EmailName.fromText input `shouldBe` Left EmailName.Empty

  it "rejects names longer than 100 characters" $
    let input = Text.replicate 101 "x"
     in EmailName.fromText input `shouldBe` Left EmailName.LongerThan100Characters
