module Discoslab.Webapi.Contacts.MessageSpec
  ( FakeMessage (..)
  , spec
  ) where

import Discoslab.Webapi.Contacts.Message (Message)
import Discoslab.Webapi.Contacts.Message qualified as Message

import Control.Monad (replicateM)
import Data.Foldable (for_)
import Data.Maybe (mapMaybe)
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.Hspec.QuickCheck (prop)
import Test.QuickCheck qualified as QuickCheck


newtype ValidInput = ValidInput Text
  deriving (Show, Eq)


instance QuickCheck.Arbitrary ValidInput where
  arbitrary = do
    size <- do
      actualSize <- fmap (* 10) QuickCheck.getSize
      pure $
        if actualSize <= 0
          then 1
          else actualSize

    nameLengths <- do
      let genLength remainingSize lengths
            | remainingSize <= 0 = pure lengths
            | otherwise = do
                let isFirstWord = null lengths

                    upper
                      | isFirstWord = remainingSize
                      | remainingSize <= 1 = 1
                      | otherwise = remainingSize - 1

                    decreaseRemaining n
                      | isFirstWord = remainingSize - n
                      | otherwise = remainingSize - n - 1

                nameLength <- QuickCheck.chooseInt (1, upper)

                genLength (decreaseRemaining nameLength) (nameLength : lengths)

      genLength size []

    names <- do
      let genChar =
            QuickCheck.elements . mconcat $
              [ ['a' .. 'z']
              , ['A' .. 'Z']
              , ['0' .. '9']
              , "`~!@#$%^&*()_+\\|/?.>,<'"
              ]

          genName n =
            Text.pack <$> replicateM n genChar

      traverse genName nameLengths

    pure . ValidInput $ Text.unwords names


newtype FakeMessage = FakeMessage {value :: Message}
  deriving (Show, Eq)


instance QuickCheck.Arbitrary FakeMessage where
  arbitrary = do
    QuickCheck.suchThatMap QuickCheck.arbitrary $ \(ValidInput text) ->
      case Message.fromText text of
        Right value -> Just $ FakeMessage value
        _ -> Nothing


  shrink (FakeMessage fakeEmail) =
    let shrunkenInputs = QuickCheck.shrink . ValidInput $ Message.toText fakeEmail
     in flip mapMaybe shrunkenInputs $ \(ValidInput text) ->
          case Message.fromText text of
            Right value -> Just $ FakeMessage value
            _ -> Nothing


spec :: Spec
spec = do
  prop "accepts all valid input" $ \(ValidInput input) ->
    let result = Message.toText <$> Message.fromText input
        expected = Right $ Text.strip input
     in result `shouldBe` expected

  prop "accepts all valid input with trailing or leading whitespace" $
    \(ValidInput input, QuickCheck.NonNegative leading, QuickCheck.NonNegative trailing) ->
      let
        result =
          fmap Message.toText . Message.fromText . mconcat $
            [ Text.replicate leading " "
            , input
            , Text.replicate trailing " "
            ]
        expected = Right $ Text.strip input
       in
        result `shouldBe` expected

  describe "rejects empty strings" $
    let examples =
          [ ""
          , "\t"
          , "\n"
          , " "
          , " \t\n "
          ]
     in for_ examples $ \input ->
          it (show input) $
            Message.fromText input `shouldBe` Left Message.Empty

  it "rejects names longer than 1000 characters" $
    let input = Text.replicate 1001 "x"
     in Message.fromText input `shouldBe` Left Message.LongerThan1000Characters
