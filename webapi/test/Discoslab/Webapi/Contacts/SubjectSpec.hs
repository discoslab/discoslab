module Discoslab.Webapi.Contacts.SubjectSpec
  ( FakeSubject (..)
  , spec
  ) where

import Discoslab.Webapi.Contacts.Subject (Subject)
import Discoslab.Webapi.Contacts.Subject qualified as Subject

import Control.Monad (replicateM)
import Data.Foldable (for_)
import Data.Maybe (mapMaybe)
import Data.Text (Text)
import Data.Text qualified as Text
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.Hspec.QuickCheck (prop)
import Test.QuickCheck qualified as QuickCheck


newtype ValidInput = ValidInput Text
  deriving (Show, Eq)


instance QuickCheck.Arbitrary ValidInput where
  arbitrary = do
    size <- do
      actualSize <- QuickCheck.getSize
      pure $
        if actualSize <= 0
          then 1
          else actualSize

    nameLengths <- do
      let genLength remainingSize lengths
            | remainingSize <= 0 = pure lengths
            | otherwise = do
                let isFirstWord = null lengths

                    upper
                      | isFirstWord = remainingSize
                      | remainingSize <= 1 = 1
                      | otherwise = remainingSize - 1

                    decreaseRemaining n
                      | isFirstWord = remainingSize - n
                      | otherwise = remainingSize - n - 1

                nameLength <- QuickCheck.chooseInt (1, upper)

                genLength (decreaseRemaining nameLength) (nameLength : lengths)

      genLength size []

    names <- do
      let genChar =
            QuickCheck.elements . mconcat $
              [ ['a' .. 'z']
              , ['A' .. 'Z']
              , ['0' .. '9']
              , "`~!@#$%^&*()_+\\|/?.>,<'"
              ]

          genName n =
            Text.pack <$> replicateM n genChar

      traverse genName nameLengths

    pure . ValidInput $ Text.unwords names


newtype FakeSubject = FakeSubject {value :: Subject}
  deriving (Show, Eq)


instance QuickCheck.Arbitrary FakeSubject where
  arbitrary = do
    QuickCheck.suchThatMap QuickCheck.arbitrary $ \(ValidInput text) ->
      case Subject.fromText text of
        Right value -> Just $ FakeSubject value
        _ -> Nothing


  shrink (FakeSubject fakeEmail) =
    let shrunkenInputs = QuickCheck.shrink . ValidInput $ Subject.toText fakeEmail
     in flip mapMaybe shrunkenInputs $ \(ValidInput text) ->
          case Subject.fromText text of
            Right value -> Just $ FakeSubject value
            _ -> Nothing


spec :: Spec
spec = do
  prop "accepts all valid input" $ \(ValidInput input) ->
    let result = Subject.toText <$> Subject.fromText input
        expected = Right input
     in result `shouldBe` expected

  prop "accepts all valid input with trailing or leading whitespace" $
    \(ValidInput input, QuickCheck.NonNegative leading, QuickCheck.NonNegative trailing) ->
      let
        result =
          fmap Subject.toText . Subject.fromText . mconcat $
            [ Text.replicate leading " "
            , input
            , Text.replicate trailing " "
            ]
        expected = Right input
       in
        result `shouldBe` expected

  describe "rejects empty strings" $
    let examples =
          [ ""
          , "\t"
          , "\n"
          , " "
          , " \t\n "
          ]
     in for_ examples $ \input ->
          it (show input) $
            Subject.fromText input `shouldBe` Left Subject.Empty

  it "rejects subjects longer than 100 characters" $
    let input = Text.replicate 101 "x"
     in Subject.fromText input `shouldBe` Left Subject.LongerThan100Characters
