module Discoslab.Webapi.Test.App
  ( withStubbedData
  , TestApp (..)
  , with
  )
where

import Discoslab.Common.Environment (bool)
import Discoslab.Logging.Config qualified as LoggingConfig
import Discoslab.Logging.Data qualified as LoggingData
import Discoslab.Webapi.Admin.Emails qualified as AdminEmails
import Discoslab.Webapi.AppData (AppData (AppData))
import Discoslab.Webapi.AppData qualified as AppData
import Discoslab.Webapi.AppM qualified as AppM
import Discoslab.Webapi.Config qualified as Config
import Discoslab.Webapi.Email.Address qualified as EmailAddress
import Discoslab.Webapi.Email.From (EmailFrom (EmailFrom))
import Discoslab.Webapi.Email.From qualified as EmailFrom
import Discoslab.Webapi.Email.Name qualified as EmailName
import Discoslab.Webapi.Http qualified as Http
import Discoslab.Webapi.Http.EnableSwagger (EnableSwagger (NoSwagger))
import Discoslab.Webapi.Test.Stubs.Email qualified as EmailStubs

import Control.Applicative (Alternative (..))
import Control.Exception (bracket)
import Control.Monad.Cont (ContT (..))
import Control.Monad.IO.Class (MonadIO (..))
import Data.Char qualified as Char
import Env qualified
import Network.Wai.Handler.Warp qualified as Warp


newtype TestConfig = TestConfig
  { enableLogs :: Bool
  }


testConfigFromEnv :: IO TestConfig
testConfigFromEnv =
  Env.parse (Env.header "discoslab-webapi-test") $ do
    let
      allowLogsVar =
        let
          info =
            mconcat
              [ Env.help "Enable logs when running tests."
              , Env.def False
              , Env.helpDef (fmap Char.toLower . show)
              ]
         in
          Env.var bool "ENABLE_LOGS" info <|> pure False

    Env.prefixed "DISCOSLAB_WEBAPI_TEST_" $
      fmap TestConfig allowLogsVar


-- | The goal is to construct the most benign 'AppData' version as possible.
-- And then let the caller update the returned 'AppData' if they want more advanced stubs.
withStubbedData :: (AppData -> IO a) -> IO a
withStubbedData use = do
  normalConfig <- Config.fromEnv
  testConfig <- testConfigFromEnv

  let
    withLoggingData
      | testConfig.enableLogs =
          LoggingData.withStdout "discoslab-webapi-test" normalConfig.logging
      | otherwise =
          bracket (LoggingData.noLogs normalConfig.logging.metadata) (\_ -> pure ())

  withLoggingData $ \logging -> do
    fromAddress <- do
      Right name <- pure $ EmailName.fromText "Test Suite"
      Right address <- pure $ EmailAddress.fromText "test.suite@example.com"
      pure EmailFrom{name = Just name, address}

    use
      AppData
        { logging
        , fromAddress
        , adminEmails = AdminEmails.def
        , sendEmail = EmailStubs.discardSentEmail
        }


-- | Information for interacting with our test application.
data TestApp = TestApp
  { httpPort :: Warp.Port
  , appData :: AppData
  }


-- | Start a new test application.
with :: AppData -> (TestApp -> IO a) -> IO a
with appData =
  runContT $ do
    waiApp <-
      liftIO . AppM.toIO appData $
        Http.makeWaiApplication NoSwagger ""

    httpPort <- ContT $ Warp.testWithApplication (pure waiApp)

    pure $ TestApp{..}
