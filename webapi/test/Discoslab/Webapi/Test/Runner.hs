module Discoslab.Webapi.Test.Runner (run) where

import System.Timeout (timeout)
import Test.Hspec
import Test.Hspec.QuickCheck (modifyMaxSize)


addTimeout :: IO () -> IO ()
addTimeout specItem = do
  result <- timeout 10_000_000 specItem

  case result of
    Nothing -> fail "Spec item took longer than 10 seconds."
    Just () -> pure ()


run :: Spec -> IO ()
run =
  hspec
    . modifyMaxSize (const 100)
    . parallel
    . around_ addTimeout
