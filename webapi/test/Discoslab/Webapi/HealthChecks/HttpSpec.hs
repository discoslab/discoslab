module Discoslab.Webapi.HealthChecks.HttpSpec (spec) where

import Discoslab.Webapi.Test.App qualified as TestApp

import Network.HTTP.Client qualified as Http
import Network.HTTP.Types.Status qualified as HttpStatus
import Test.Hspec (Spec, describe, it, runIO, shouldBe)


spec :: Spec
spec = do
  manager <- runIO $ Http.newManager Http.defaultManagerSettings

  describe "GET ~/health-checks" $ do
    it "always returns a 204" $ do
      TestApp.withStubbedData $ \appData ->
        TestApp.with appData $ \app -> do
          let url = "http://localhost:" <> show app.httpPort <> "/health-checks"

          request <- Http.parseRequest url
          response <- Http.httpLbs request manager

          Http.responseStatus response `shouldBe` HttpStatus.status204
          Http.responseBody response `shouldBe` mempty
