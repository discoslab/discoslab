module Main (main) where

import Discoslab.Webapi.Test.Runner (run)
import Spec (spec)


main :: IO ()
main = run spec
