-- | Type and functions for dealing with the subject of an email.
module Discoslab.Webapi.Contacts.Subject
  ( Subject
  , Error (..)
  , errorText
  , fromText
  , toText
  )
where

import Discoslab.Common.BoundedEnum qualified as BoundedEnum

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (..))
import Data.Text (Text)
import Data.Text qualified as Text


-- | Subject of an email. Such as "Welcome to the Disco's lab!".
newtype Subject = Subject Text
  deriving (Show, Eq)


-- | Error describing why a 'Subject' could not be constructed.
data Error
  = Empty
  | LongerThan100Characters
  deriving (Show, Eq, Ord, Enum, Bounded)


-- | Convert an 'Error' to a human suitable 'Text'.
errorText :: Error -> Text
errorText = \case
  Empty -> "Subject is empty."
  LongerThan100Characters -> "Subject is longer than 100 characters."


-- | Convert from 'Text' to an 'Subject'.
-- See 'Error' for reasons why this would return a 'Left' variant.
fromText :: Text -> Either Error Subject
fromText text
  | Text.null strippedText = Left Empty
  | Text.length strippedText > 100 = Left LongerThan100Characters
  | otherwise = Right $ Subject strippedText
 where
  strippedText = Text.strip text


-- | Convert from an 'Subject' to 'Text'.
toText :: Subject -> Text
toText =
  coerce


instance Aeson.ToJSON Error where
  toJSON = Aeson.toJSON . errorText
  toEncoding = Aeson.toEncoding . errorText


instance Aeson.FromJSON Error where
  parseJSON =
    BoundedEnum.parseJSON errorText


instance OpenApi.ToSchema Error where
  declareNamedSchema _ =
    pure . OpenApi.NamedSchema (Just "ContactsSubjectError") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.description ?~ "Error message describing why an email subject was rejected."
        & OpenApi.example ?~ Aeson.toJSON Empty
        & OpenApi.enum_ ?~ fmap (Aeson.toJSON @Error) [minBound ..]


instance Aeson.ToJSON Subject where
  toJSON = Aeson.toJSON . toText
  toEncoding = Aeson.toEncoding . toText


instance Aeson.FromJSON Subject where
  parseJSON = Aeson.withText "ContactsSubject" $ \text ->
    case fromText text of
      Left err -> fail . Text.unpack $ errorText err
      Right value -> pure value


instance OpenApi.ToSchema Subject where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "ContactsSubject") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.description ?~ "Subject of an email message."
        & OpenApi.example ?~ Aeson.toJSON @Text "How are you?"
