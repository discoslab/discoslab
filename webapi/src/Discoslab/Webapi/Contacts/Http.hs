-- | HTTP handlers for @~/contacts@.
module Discoslab.Webapi.Contacts.Http
  ( Api
  , server
  )
where

import Discoslab.Webapi.AppM (AppM)
import Discoslab.Webapi.Contacts.Http.Post qualified as Post

import GHC.Stack (HasCallStack)
import Servant (HasServer (ServerT))


type Api =
  Post.Api


server :: HasCallStack => ServerT Api AppM
server =
  Post.server
