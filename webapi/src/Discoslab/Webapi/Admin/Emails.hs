-- | Emails of the website admins. Mostly so we can pester them with emails.
module Discoslab.Webapi.Admin.Emails
  ( AdminEmails (..)
  , def
  , var
  )
where

import Discoslab.Common.Environment qualified as SharedEnv
import Discoslab.Webapi.Email.Address (EmailAddress)
import Discoslab.Webapi.Email.Address qualified as EmailAddress

import Data.Foldable (Foldable (toList))
import Data.List.NonEmpty (NonEmpty, nonEmpty)
import Data.Text qualified as Text
import Env qualified


-- | Email addresses for the website admins.
newtype AdminEmails = AdminEmails (NonEmpty EmailAddress)
  deriving (Show, Eq)


-- | Default list of admin emails. Defaults to "webapi@discoslab.com".
def :: AdminEmails
def =
  AdminEmails $ pure EmailAddress.webapiAtDiscoslabDotCom


-- | Parse 'AdminEmails' from an environment variable. Emails should be comma separated.
var :: Env.Parser Env.Error AdminEmails
var =
  let adminEmails =
        SharedEnv.parseString $ \text -> do
          let unvalidatedEmails =
                [ unvalidatedEmail
                | item <- Text.split (== ',') text
                , let unvalidatedEmail = Text.strip item
                , not $ Text.null unvalidatedEmail
                ]

              parseEmail =
                either (const Nothing) Just . EmailAddress.fromText

          emails <- traverse parseEmail unvalidatedEmails

          AdminEmails <$> nonEmpty emails

      toString (AdminEmails emails) =
        Text.unpack . Text.intercalate "," . toList $
          fmap EmailAddress.toText emails
   in Env.var adminEmails "ADMIN_EMAILS" . mconcat $
        [ Env.help "Comma separated list of email addresses for the website admins."
        , Env.def def
        , Env.helpDef toString
        ]
