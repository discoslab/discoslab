-- | Type and functions for dealing with the application's monad.
module Discoslab.Webapi.AppM
  ( AppM (..)
  , toIO
  , toHandler
  )
where

import Discoslab.Logging.Data qualified as LoggingData
import Discoslab.Webapi.AppData (AppData)
import Discoslab.Webapi.AppData qualified as AppData

import Control.Exception.Annotated.UnliftIO (catch, throw, try)
import Control.Exception.Safe (MonadCatch, MonadMask, MonadThrow)
import Control.Monad.Except (ExceptT (ExceptT), MonadError (..))
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.IO.Unlift (MonadUnliftIO)
import Control.Monad.Reader (MonadReader (local), ReaderT (..), asks)
import Data.Coerce (coerce)
import Katip (Katip (..), KatipContext (..))
import Servant.Server qualified as Servant


-- | The application monad.
newtype AppM a = AppM (ReaderT AppData IO a)
  deriving
    ( Functor
    , Applicative
    , Monad
    , MonadIO
    , MonadUnliftIO
    , MonadThrow
    , MonadMask
    , MonadCatch
    , MonadReader AppData
    )


-- | Convert 'AppM' to 'IO' using the 'AppData'.
toIO :: AppData -> AppM a -> IO a
toIO appData (AppM app) =
  runReaderT app appData


-- | Convert 'AppM' to 'Servant.Handler' using the 'AppData'.
-- **WARNING** This will catch all 'Servant.ServerError' exceptions.
toHandler :: AppData -> AppM a -> Servant.Handler a
toHandler appData =
  coerce . try @Servant.ServerError . toIO appData


-- | Exists solely for using 'AppM' from within servant handlers.
instance MonadError Servant.ServerError AppM where
  throwError = throw
  catchError = catch


instance Katip.Katip AppM where
  getLogEnv =
    asks (.logging.env)


  localLogEnv f =
    local $ \appData ->
      let updatedLogging =
            appData.logging
              { LoggingData.env = f appData.logging.env
              }
       in appData{AppData.logging = updatedLogging}


instance Katip.KatipContext AppM where
  getKatipContext =
    asks (.logging.contexts)


  localKatipContext f =
    local $ \appData ->
      let updatedLogging =
            appData.logging
              { LoggingData.contexts = f appData.logging.contexts
              }
       in appData{AppData.logging = updatedLogging}


  getKatipNamespace =
    asks (.logging.namespace)


  localKatipNamespace f =
    local $ \appData ->
      let updatedLogging =
            appData.logging
              { LoggingData.namespace = f appData.logging.namespace
              }
       in appData{AppData.logging = updatedLogging}
