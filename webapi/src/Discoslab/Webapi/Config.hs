-- | Type and functions for configuring the application.
module Discoslab.Webapi.Config
  ( Config (..)
  , vars
  , fromEnv
  )
where

import Discoslab.Logging.Config (LoggingConfig)
import Discoslab.Logging.Config qualified as LoggingConfig
import Discoslab.Logging.Metadata.InstanceId (InstanceId)
import Discoslab.Logging.Metadata.InstanceId qualified as InstanceId
import Discoslab.Logging.Metadata.StartedAt (StartedAt)
import Discoslab.Logging.Metadata.StartedAt qualified as StartedAt
import Discoslab.Webapi.Admin.Emails (AdminEmails)
import Discoslab.Webapi.Admin.Emails qualified as AdminEmails
import Discoslab.Webapi.Http.Config (HttpConfig)
import Discoslab.Webapi.Http.Config qualified as HttpConfig
import Discoslab.Webapi.Smtp.Config (SmtpConfig)
import Discoslab.Webapi.Smtp.Config qualified as SmtpConfig

import Control.Monad (when)
import Control.Monad.IO.Class (MonadIO (..))
import Data.Set qualified as Set
import Data.Text qualified as Text
import Env qualified
import System.Environment (getArgs)
import System.Exit (exitSuccess)


-- | Configuration for the entire applicaiton.
data Config = Config
  { logging :: LoggingConfig
  -- ^ Configuration for setting up logging.
  , http :: HttpConfig
  -- ^ Configuration for the REST API.
  , smtp :: SmtpConfig
  -- ^ Configuration for sending emails with SMTP.
  , adminEmails :: AdminEmails
  -- ^ Emails of the website's admins.
  }
  deriving (Show, Eq)


-- | Parse the 'Config' from environment variables.
vars :: InstanceId -> StartedAt -> Env.Parser Env.Error Config
vars instanceId startedAt =
  Env.prefixed "DISCOSLAB_WEBAPI_" $
    (\l -> Config (l instanceId startedAt))
      <$> LoggingConfig.vars
      <*> HttpConfig.vars
      <*> SmtpConfig.vars
      <*> AdminEmails.var


-- | Checks if @-h@ or @--help@ are present in the list.
-- Trailing whitespace, leading whitespace, and case are all ignored.
hasHelpFlag :: [String] -> Bool
hasHelpFlag args =
  let allFlags =
        Set.fromList $ fmap (Text.toCaseFold . Text.strip . Text.pack) args

      helpFlags =
        Set.fromList
          [ Text.toCaseFold "-h"
          , Text.toCaseFold "--help"
          ]
   in not $ Set.disjoint helpFlags allFlags


-- | Parse the 'Config' from environment variables and print a help message if any fail.
fromEnv :: MonadIO m => m Config
fromEnv = do
  instanceId <- InstanceId.nextRandom
  startedAt <- StartedAt.now

  let parser = vars instanceId startedAt

  liftIO $ do
    args <- getArgs

    when (hasHelpFlag args) $ do
      putStrLn $ Env.helpDoc parser
      exitSuccess

    Env.parse (Env.header "discoslab-webapi") parser
