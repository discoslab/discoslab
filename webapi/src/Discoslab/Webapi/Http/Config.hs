-- | Type and configuration for our http server.
module Discoslab.Webapi.Http.Config
  ( HttpConfig (..)
  , def
  , vars
  )
where

import Discoslab.Webapi.Http.EnableSwagger (EnableSwagger)
import Discoslab.Webapi.Http.EnableSwagger qualified as EnableSwagger

import Env qualified
import Network.Wai.Handler.Warp qualified as Warp


-- | Configuration settings for our http server.
data HttpConfig = HttpConfig
  { port :: Warp.Port
  -- ^ Port that the WAI application will run on.
  , enableSwagger :: EnableSwagger
  -- ^ If true, swagger ui will be available under /docs.
  , openApiBasePath :: FilePath
  -- ^ Base path of the API, so we can correct all of the routes in the OpenApi json.
  -- For example if the api is hosted at our-cool-website.com/api, we would set the base path to /api.
  }
  deriving (Show, Eq)


-- | Default http settings.
def :: HttpConfig
def =
  HttpConfig
    { port = 3000
    , enableSwagger = EnableSwagger.def
    , openApiBasePath = ""
    }


-- | Parse a 'Warp.Port' from an environment variable.
portVar :: Env.Parser Env.Error Warp.Port
portVar =
  Env.var Env.auto "PORT" . mconcat $
    [ Env.help "Port the HTTP server should run on."
    , Env.def def.port
    , Env.helpDef show
    ]


openApiBasePathVar :: Env.Parser Env.Error FilePath
openApiBasePathVar =
  Env.var Env.str "OPENAPI_BASE_PATH" . mconcat $
    [ Env.help "Base path for the openapi.json document."
    , Env.def def.openApiBasePath
    , Env.helpDef id
    ]


-- | Parse all of the 'HttpConfig' from environment variables.
vars :: Env.Parser Env.Error HttpConfig
vars =
  Env.prefixed "HTTP_" $
    HttpConfig
      <$> portVar
      <*> EnableSwagger.var
      <*> openApiBasePathVar
