-- | Glues all of our HTTP routes together.
module Discoslab.Webapi.Http.Routes
  ( Api
  , server
  , openApi
  , ApiWithSwagger
  , serverWithSwagger
  )
where

import Discoslab.Webapi.Alerts.Http qualified as Alerts
import Discoslab.Webapi.Contacts.Http qualified as Contacts
import Discoslab.Webapi.HealthChecks.Http qualified as HealthChecks

import Discoslab.Logging.Metadata.Version (Version)
import Discoslab.Logging.Metadata.Version qualified as Version
import Discoslab.Webapi.AppM (AppM)

import Control.Lens ((%~), (.~), (?~))
import Data.Function ((&))
import Data.HashMap.Strict.InsOrd qualified as InsOrd
import Data.List (isPrefixOf)
import Data.OpenApi (OpenApi)
import Data.OpenApi qualified as OpenApi
import Data.OpenApi.Operation (applyTagsFor)
import Debug.Trace qualified as Debug
import GHC.Stack (HasCallStack)
import Servant (HasServer (ServerT), Proxy (Proxy), type (:<|>) (..), type (:>))
import Servant.OpenApi (subOperations, toOpenApi)
import Servant.Swagger.UI (SwaggerSchemaUI, swaggerSchemaUIServerT)
import System.FilePath ((</>))


type HealthChecks = "health-checks" :> HealthChecks.Api
type Alerts = "alerts" :> Alerts.Api
type Contacts = "contacts" :> Contacts.Api


-- | Type describing our web api.
type Api =
  HealthChecks
    :<|> Alerts
    :<|> Contacts


-- | Server for handling requests that match the 'Api' type.
server :: HasCallStack => ServerT Api AppM
server =
  HealthChecks.server
    :<|> Alerts.server
    :<|> Contacts.server


-- | Type describing our web api in addition to serving swagger docs under @/docs@.
type ApiWithSwagger =
  SwaggerSchemaUI "docs" "openapi.json" :<|> Api


-- | 'OpenApi' documentation for our api.
openApi :: Version -> FilePath -> OpenApi
openApi version basePath =
  let tagRoutes proxy name description =
        applyTagsFor
          (subOperations @_ @Api proxy Proxy)
          [name & OpenApi.description ?~ description]

      modifyPath path
        | basePath == "" || basePath == "/" = path
        | otherwise =
            let modifiedBasePath = basePath </> dropWhile (== '/') path
             in if "/" `isPrefixOf` modifiedBasePath
                  then modifiedBasePath
                  else "/" </> modifiedBasePath
   in Debug.trace basePath $
        toOpenApi @Api Proxy
          & OpenApi.info . OpenApi.title .~ "discoslab-webapi"
          & OpenApi.info . OpenApi.description ?~ "Public REST API used by the Disco's lab website"
          & OpenApi.info . OpenApi.version .~ Version.toText version
          & tagRoutes (Proxy @HealthChecks) "Health Checks" "End points for checking the health of the REST API"
          & tagRoutes (Proxy @Alerts) "Alerts" "End points for sending alerts from the front end."
          & tagRoutes (Proxy @Contacts) "Contacts" "End points for getting in contact with me."
          & OpenApi.paths %~ InsOrd.mapKeys modifyPath


-- | Server for handling requests that match the 'Api' type as well as serving the swagger docs under @/docs@.
serverWithSwagger :: HasCallStack => Version -> FilePath -> ServerT ApiWithSwagger AppM
serverWithSwagger version basePath =
  swaggerSchemaUIServerT (openApi version basePath) :<|> server
