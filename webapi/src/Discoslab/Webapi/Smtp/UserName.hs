-- | Type and functions for the login user name related to a smtp server.
module Discoslab.Webapi.Smtp.UserName
  ( UserName
  , Error (..)
  , errorText
  , fromText
  , toText
  , var
  )
where

import Discoslab.Common.Environment (parseString)

import Data.Coerce (coerce)
import Data.Text (Text)
import Data.Text qualified as Text
import Env qualified


-- | User name for logging into a smtp server.
newtype UserName = UserName Text
  deriving (Show, Eq)


-- | An error describing why a user name was rejected.
data Error
  = Empty
  deriving (Show, Eq, Ord, Enum, Bounded)


-- | Convert the 'Error' that to a human suitable 'Text'.
errorText :: Error -> Text
errorText = \case
  Empty -> "SMTP user name is empty."


-- | Parse the 'UserName' from a 'Text'.
-- See 'Error' for reasons why this may return the 'Left' variant.
fromText :: Text -> Either Error UserName
fromText text
  | Text.null strippedText = Left Empty
  | otherwise = Right $ UserName strippedText
 where
  strippedText = Text.strip text


-- | Convert the 'UserName' to 'Text'.
toText :: UserName -> Text
toText =
  coerce


-- | Parse 'UserName' from an environment variable.
var :: Env.Parser Env.Error UserName
var =
  let userName =
        parseString $ \text ->
          case fromText text of
            Left _ -> Nothing
            Right value -> Just value
   in Env.var userName "USER_NAME" . mconcat $
        [ Env.help . Text.unpack $
            "User name for logging into a SMTP server."
        ]
