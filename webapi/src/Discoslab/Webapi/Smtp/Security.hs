-- | Type and functions for setting the security protocol when connecting a SMTP server.
module Discoslab.Webapi.Smtp.Security
  ( Security (..)
  , def
  , toText
  , fromText
  , var
  )
where

import Discoslab.Common.BoundedEnum qualified as BoundedEnum
import Discoslab.Common.Environment (parseString)

import Data.Text (Text)
import Data.Text qualified as Text
import Env qualified


-- | The security protocol when connecting a SMTP server.
data Security
  = None
  | Tls
  | Starttls
  deriving (Show, Eq, Enum, Bounded)


-- | Convert 'Security' to 'Text'.
toText :: Security -> Text
toText = \case
  None -> "none"
  Tls -> "tls"
  Starttls -> "starttls"


-- | Convert a 'Security' from 'Text'. Unrecognized text will result in a 'Nothing'.
fromText :: Text -> Maybe Security
fromText =
  BoundedEnum.fromText toText


-- | Default 'Security' protocol. Currenlty set to 'None'.
def :: Security
def =
  None


-- | Parse 'Security' from an environment variable.
var :: Env.Parser Env.Error Security
var =
  let validValues = Text.intercalate ", " $ fmap toText [minBound @Security ..]
   in Env.var (parseString fromText) "SECURITY" . mconcat $
        [ Env.help . Text.unpack $
            "Security protocol to use when connecting a SMTP server. Valid valies are: " <> validValues
        , Env.def def
        , Env.helpDef (Text.unpack . toText)
        ]
