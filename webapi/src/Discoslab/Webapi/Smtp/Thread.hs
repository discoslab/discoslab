-- | Module for creating a background thread for sending out emails.
module Discoslab.Webapi.Smtp.Thread
  ( SmtpThread
  , with
  , send
  )
where

import Discoslab.Logging.Exceptions (logException)
import Discoslab.Webapi.Email (Email)
import Discoslab.Webapi.Email qualified as Email
import Discoslab.Webapi.Smtp.Config (SmtpConfig)
import Discoslab.Webapi.Smtp.Config qualified as Config
import Discoslab.Webapi.Smtp.Login qualified as Login
import Discoslab.Webapi.Smtp.Password qualified as Password
import Discoslab.Webapi.Smtp.Security qualified as Security
import Discoslab.Webapi.Smtp.UserName qualified as UserName

import Control.Concurrent.STM qualified as STM
import Control.Concurrent.STM.TBQueue (TBQueue)
import Control.Concurrent.STM.TBQueue qualified as TBQueue
import Control.Exception (SomeException, bracket)
import Control.Exception.Annotated.UnliftIO (checkpointCallStack, tryAnnotated)
import Control.Immortal qualified as Immortal
import Control.Monad.IO.Class (MonadIO (..))
import Control.Monad.IO.Unlift (MonadUnliftIO (..))
import Data.Text qualified as Text
import GHC.Stack (HasCallStack)
import Katip qualified
import Network.Mail.SMTP qualified as SMTP


data Message
  = Close
  | SendEmail Email
  deriving (Show, Eq)


type Queue = TBQueue Message


-- | A background SMTP thread.
newtype SmtpThread = SmtpThread
  { queue :: Queue
  }


-- TODO Add retry logic
sendEmailViaSmtp
  :: ( HasCallStack
     , MonadUnliftIO m
     , Katip.KatipContext m
     )
  => SmtpConfig
  -> Email
  -> m ()
sendEmailViaSmtp config email =
  checkpointCallStack $ do
    Katip.logLocM Katip.DebugS "Attempting to send an email."

    let mimeMail =
          Email.toMimeMail email

    result <-
      tryAnnotated @SomeException . liftIO $ case config.login of
        Nothing ->
          let yeet smtpFunction =
                smtpFunction config.hostName config.port mimeMail
           in yeet $ case config.security of
                Security.None -> SMTP.sendMail'
                Security.Tls -> SMTP.sendMailTLS'
                Security.Starttls -> SMTP.sendMailSTARTTLS'
        Just login ->
          let userName =
                Text.unpack $ UserName.toText login.userName
              password =
                Text.unpack $ Password.toText login.password
              yeet smtpFunction =
                smtpFunction config.hostName config.port userName password mimeMail
           in yeet $ case config.security of
                Security.None -> SMTP.sendMailWithLogin'
                Security.Tls -> SMTP.sendMailWithLoginTLS'
                Security.Starttls -> SMTP.sendMailWithLoginSTARTTLS'

    case result of
      Left ex ->
        logException Katip.CriticalS ex "Email was unable to be sent!"
      Right () ->
        Katip.logLocM Katip.InfoS "Email was successfully sent."


startLoop
  :: ( HasCallStack
     , MonadUnliftIO m
     , Katip.KatipContext m
     )
  => SmtpConfig
  -> Queue
  -> m ()
startLoop config queue =
  let loop = do
        message <- liftIO . STM.atomically $ TBQueue.readTBQueue queue

        case message of
          Close ->
            Katip.logLocM Katip.InfoS "All emails have processed. Shutting down."
          SendEmail email -> do
            Katip.logLocM Katip.DebugS "Received an email to send out."
            sendEmailViaSmtp config email
            loop
   in checkpointCallStack loop


-- | Construct a 'SmtpThread' that you can send emails with using the 'send' function.
with
  :: ( HasCallStack
     , MonadUnliftIO m
     , Katip.KatipContext m
     )
  => SmtpConfig
  -> (SmtpThread -> m a)
  -> m a
with config use =
  checkpointCallStack . Katip.katipAddNamespace "smtp" $ do
    Katip.logLocM Katip.DebugS $ "Creating a bounded queue with the size: " <> Katip.showLS config.queueSize
    queue <- liftIO $ TBQueue.newTBQueueIO config.queueSize

    withRunInIO $ \runInIO -> do
      let create = checkpointCallStack $ do
            runInIO $ Katip.logLocM Katip.InfoS "Starting the SMTP thread."
            Immortal.createWithLabel "SmtpThread" (\_ -> runInIO $ startLoop config queue)

          cleanup thread = checkpointCallStack $ do
            runInIO $ Katip.logLocM Katip.InfoS "Sending signal to shutdown the SMTP thread."

            Immortal.mortalize thread
            STM.atomically $ TBQueue.writeTBQueue queue Close

            runInIO $ Katip.logLocM Katip.InfoS "Waiting for SMTP thread to finish and shutdown."
            Immortal.wait thread

      bracket create cleanup $ \_ ->
        runInIO . checkpointCallStack $ use (SmtpThread queue)


-- | Send an email to the background thread so it can mail it out using SMTP.
send :: (HasCallStack, MonadUnliftIO m, Katip.KatipContext m) => SmtpThread -> Email -> m ()
send thread email =
  checkpointCallStack $ do
    liftIO . STM.atomically . TBQueue.writeTBQueue thread.queue $!
      SendEmail email

    Katip.logLocM Katip.DebugS "Email queued to be sent."
