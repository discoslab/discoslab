-- | End points for the checking on the health of the API.
module Discoslab.Webapi.HealthChecks.Http
  ( Get
  , get
  , Api
  , server
  )
where

import Discoslab.Webapi.AppM (AppM)

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import GHC.Stack (HasCallStack)
import Servant
  ( Description
  , GetNoContent
  , HasServer (ServerT)
  , NoContent (NoContent)
  , Summary
  , type (:>)
  )


type Get =
  Summary "Is this API reachable?"
    :> Description "Allows you to check if the API is reachable. Hardcoded to return 204."
    :> GetNoContent


get :: HasCallStack => AppM NoContent
get =
  checkpointCallStack $ pure NoContent


type Api =
  Get


server :: HasCallStack => ServerT Api AppM
server =
  get
