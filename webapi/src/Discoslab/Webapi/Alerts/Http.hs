module Discoslab.Webapi.Alerts.Http
  ( Api
  , server
  )
where

import Discoslab.Webapi.Alerts.Http.Post qualified as Post
import Discoslab.Webapi.AppM (AppM)

import GHC.Stack (HasCallStack)
import Servant (HasServer (ServerT))


type Api =
  Post.Api


server :: HasCallStack => ServerT Api AppM
server =
  Post.server
