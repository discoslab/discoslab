module Discoslab.Webapi.Alerts.Http.Post
  ( Api
  , server
  ) where

import Discoslab.Webapi.Alerts.Http.Post.Request (PostRequest)
import Discoslab.Webapi.Alerts.Http.Post.Request qualified as PostRequest
import Discoslab.Webapi.AppM (AppM)

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import GHC.Stack (HasCallStack)
import Katip qualified
import Servant
  ( Description
  , HasServer (ServerT)
  , JSON
  , NoContent (NoContent)
  , PostNoContent
  , ReqBody
  , Summary
  , type (:>)
  )


type Api =
  Summary "Log an alert from the frontend."
    :> Description "This end point exists so that the frontend can record logs if something bad happens."
    :> ReqBody '[JSON] PostRequest
    :> PostNoContent


server :: HasCallStack => ServerT Api AppM
server request =
  checkpointCallStack $
    let addContext = Katip.katipAddContext (Katip.sl "alertContext" request.context)
        addNamespace = Katip.katipAddNamespace . Katip.Namespace . ("alerts" :) $
          case request.namespace of
            Just (ns : nss) -> ns : nss
            _ -> mempty
     in addNamespace . addContext $ do
          Katip.logFM request.severity (Katip.ls request.message)
          pure NoContent
