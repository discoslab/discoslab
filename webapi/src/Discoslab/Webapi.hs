-- | Root of the application
module Discoslab.Webapi
  ( runAppFromConfig
  , runAppFromEnv
  , start
  )
where

import Discoslab.Logging.Exceptions (withExceptionLogging)
import Discoslab.Webapi.AppData qualified as AppData
import Discoslab.Webapi.AppM (AppM)
import Discoslab.Webapi.AppM qualified as AppM
import Discoslab.Webapi.Config (Config)
import Discoslab.Webapi.Config qualified as Config
import Discoslab.Webapi.Http qualified as Http

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import GHC.Stack (HasCallStack)


-- | Convert the 'Config' to 'AppData' and run an 'AppM' to 'IO'.
runAppFromConfig :: HasCallStack => Config -> AppM a -> IO a
runAppFromConfig config app =
  AppData.with config $ \appData ->
    -- Log unhandled exceptions as 'maxBound' because the application is about to crash!
    AppM.toIO appData (withExceptionLogging maxBound app)


-- | Pull the 'Config' from environment variables and then use 'runAppFromConfig' to convert an 'AppM' to 'IO'.
-- This exists solely to make it easier to debug from @ghci@.
runAppFromEnv :: HasCallStack => AppM a -> IO a
runAppFromEnv app = do
  config <- Config.fromEnv
  runAppFromConfig config app


-- | Starts the web api.
start :: HasCallStack => Config -> IO ()
start config =
  runAppFromConfig config . checkpointCallStack $
    Http.start config.http
