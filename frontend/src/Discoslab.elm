module Discoslab exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Browser
import Discoslab.Effect as Effect exposing (Effect)
import Discoslab.Effect.Navigation as Navigation
import Discoslab.Page as Page
import Discoslab.Ui.Layouts.Root as RootLayout
import Discoslab.Url.BasePath exposing (BasePath)
import Discoslab.Url.Route as Route exposing (Route)
import Html
import Url exposing (Url)


type alias Model =
    { basePath : BasePath
    , route : Route
    , page : Page.Model
    }


type Msg
    = UrlChanged Url
    | UrlRequested Browser.UrlRequest
    | PageMsg Page.Msg


parseUrl : BasePath -> Url -> ( Route, Page.Model )
parseUrl basePath url =
    let
        route =
            Route.parse basePath url
    in
    ( route, Page.init route )


init : BasePath -> Url -> Model
init basePath url =
    let
        ( route, page ) =
            parseUrl basePath url
    in
    { basePath = basePath
    , route = route
    , page = page
    }


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        UrlChanged url ->
            let
                ( route, page ) =
                    parseUrl model.basePath url
            in
            ( { model | route = route, page = page }
            , Effect.None
            )

        UrlRequested (Browser.Internal url) ->
            ( model
            , Url.toString url
                |> Navigation.PushUrl
                |> Effect.Navigation
            )

        UrlRequested (Browser.External url) ->
            ( model
            , Navigation.Load url
                |> Effect.Navigation
            )

        PageMsg pageMsg ->
            let
                ( page, pageEffects ) =
                    Page.update pageMsg model.page
            in
            ( { model | page = page }
            , Effect.map PageMsg pageEffects
            )


view : Model -> Browser.Document Msg
view model =
    let
        page =
            Page.view model.page

        title =
            let
                websiteName =
                    "Disco's Lab"

                trimmedPageTitle =
                    String.trim page.title
            in
            if String.isEmpty trimmedPageTitle then
                websiteName

            else
                websiteName ++ " - " ++ trimmedPageTitle

        body =
            List.map (Html.map PageMsg) page.body
                |> RootLayout.view model.basePath model.route
    in
    { title = title
    , body = body
    }
