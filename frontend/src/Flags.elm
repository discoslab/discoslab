module Flags exposing
    ( Flags
    , decode
    )

import Dict exposing (Dict)
import Discoslab.Effect.Logging as Logging
import Discoslab.Url.BasePath as BasePath exposing (BasePath)
import Json.Decode as Decode


type alias Flags =
    { webapiBasePath : BasePath
    , frontendBasePath : BasePath
    , minSeverity : Logging.Severity
    }


decode : Decode.Value -> ( Flags, Dict String Decode.Error )
decode jsonValue =
    let
        decodeFlag property default decoder =
            case Decode.decodeValue (Decode.at [ property ] decoder) jsonValue of
                Err error ->
                    ( default, Just ( property, error ) )

                Ok value ->
                    ( value, Nothing )

        ( webapiBasePath, webapiBasePathError ) =
            decodeFlag "webapiBasePath" (BasePath.fromString "/api") BasePath.decode

        ( frontendBasePath, frontendBasePathError ) =
            decodeFlag "frontendBasePath" (BasePath.fromString "/") BasePath.decode

        ( minSeverity, minSeverityError ) =
            let
                parseString string =
                    let
                        normalizedString =
                            String.toLower (String.trim string)
                    in
                    if normalizedString == "debug" then
                        Decode.succeed Logging.Debug

                    else if normalizedString == "info" then
                        Decode.succeed Logging.Info

                    else if normalizedString == "notice" then
                        Decode.succeed Logging.Notice

                    else if normalizedString == "warning" then
                        Decode.succeed Logging.Warning

                    else if normalizedString == "error" then
                        Decode.succeed Logging.Error

                    else if normalizedString == "critical" then
                        Decode.succeed Logging.Critical

                    else if normalizedString == "alert" then
                        Decode.succeed Logging.Alert

                    else if normalizedString == "emergency" then
                        Decode.succeed Logging.Emergency

                    else
                        Decode.fail "Not a valid logging severity."
            in
            decodeFlag "minSeverity" Logging.Debug (Decode.andThen parseString Decode.string)

        errors =
            [ webapiBasePathError
            , frontendBasePathError
            , minSeverityError
            ]
                |> List.filterMap identity
                |> Dict.fromList

        flags =
            { webapiBasePath = webapiBasePath
            , frontendBasePath = frontendBasePath
            , minSeverity = minSeverity
            }
    in
    ( flags, errors )
