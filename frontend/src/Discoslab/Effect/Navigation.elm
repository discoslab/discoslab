module Discoslab.Effect.Navigation exposing
    ( Effect(..)
    , toCmd
    )

import Browser.Navigation


type Effect
    = PushUrl String
    | ReplaceUrl String
    | Back Int
    | Forward Int
    | Load String
    | Reload
    | ReloadAndSkipCache


toCmd :
    { deps
        | navigationKey : Browser.Navigation.Key
    }
    -> Effect
    -> Cmd msg
toCmd deps effect =
    case effect of
        PushUrl url ->
            Browser.Navigation.pushUrl deps.navigationKey url

        ReplaceUrl url ->
            Browser.Navigation.replaceUrl deps.navigationKey url

        Back n ->
            Browser.Navigation.back deps.navigationKey n

        Forward n ->
            Browser.Navigation.forward deps.navigationKey n

        Load url ->
            Browser.Navigation.load url

        Reload ->
            Browser.Navigation.reload

        ReloadAndSkipCache ->
            Browser.Navigation.reloadAndSkipCache
