module Discoslab.Effect.Contact exposing
    ( AcceptedResponse
    , BadRequestResponse
    , Effect
    , Error
    , Request
    , map
    , toCmd
    )

import Discoslab.Contact.EmailAddress as EmailAddress exposing (EmailAddress)
import Discoslab.Contact.Message as Message exposing (Message)
import Discoslab.Contact.Name as Name exposing (Name)
import Discoslab.Contact.Subject as Subject exposing (Subject)
import Discoslab.Url.BasePath as BasePath exposing (BasePath)
import Discoslab.Utils.Http as HttpUtils
import Discoslab.Utils.Json as JsonUtils
import Http
import Json.Decode as Decode
import Json.Encode as Encode


type alias Request =
    { fromEmailAddress : Maybe EmailAddress
    , fromName : Maybe Name
    , subject : Maybe Subject
    , message : Message
    }


encodeRequest : Request -> Encode.Value
encodeRequest request =
    let
        from =
            Encode.object
                [ ( "email", JsonUtils.encodeOrNull EmailAddress.encode request.fromEmailAddress )
                , ( "name", JsonUtils.encodeOrNull Name.encode request.fromName )
                ]
    in
    Encode.object
        [ ( "from", from )
        , ( "subject", JsonUtils.encodeOrNull Subject.encode request.subject )
        , ( "message", Message.encode request.message )
        ]


type alias BadRequestResponse =
    { fromEmailAddress : Maybe String
    , fromName : Maybe String
    , subject : Maybe String
    , message : Maybe String
    }


decodeBadRequestResponse : Decode.Decoder BadRequestResponse
decodeBadRequestResponse =
    Decode.succeed BadRequestResponse
        |> JsonUtils.decodeMaybeAt [ "from", "email" ] Decode.string
        |> JsonUtils.decodeMaybeAt [ "from", "name" ] Decode.string
        |> JsonUtils.decodeMaybe "subject" Decode.string
        |> JsonUtils.decodeMaybe "message" Decode.string


type alias AcceptedResponse =
    String


decodeAcceptedResponse : Decode.Decoder AcceptedResponse
decodeAcceptedResponse =
    Decode.string


type alias Error =
    HttpUtils.ErrorBody BadRequestResponse


type alias Effect msg =
    { request : Request
    , onResponse : Result Error AcceptedResponse -> msg
    }


map : (msg1 -> msg2) -> Effect msg1 -> Effect msg2
map f effect =
    { request = effect.request
    , onResponse = f << effect.onResponse
    }


toCmd : { deps | api : BasePath } -> Effect msg -> Cmd msg
toCmd deps effect =
    let
        parseBad =
            HttpUtils.onStatus 400 decodeBadRequestResponse

        parseGood =
            HttpUtils.onStatus 202 decodeAcceptedResponse
    in
    Http.request
        { method = "post"
        , headers = []
        , url = BasePath.absolute deps.api [ "contacts" ] []
        , body = Http.jsonBody (encodeRequest effect.request)
        , expect = HttpUtils.expectJsonBodies effect.onResponse parseBad parseGood
        , timeout = Nothing
        , tracker = Nothing
        }
