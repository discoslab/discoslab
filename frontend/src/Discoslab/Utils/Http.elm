module Discoslab.Utils.Http exposing
    ( ErrorBody(..)
    , errorString
    , errorString_
    , expectJsonBodies
    , expectMetadataAndString
    , onStatus
    )

import Http
import Json.Decode as Decode exposing (Decoder)


type ErrorBody a
    = BadUrl String
    | BadResponse { metadata : Http.Metadata, body : a }
    | UnknownResponse { metadata : Http.Metadata, body : String, reason : String }
    | Timeout
    | NetworkError


errorString : (a -> String) -> ErrorBody a -> String
errorString aToString error =
    case error of
        BadUrl url ->
            "Bad url: " ++ url

        UnknownResponse { metadata, body } ->
            "Unknown response (status: " ++ String.fromInt metadata.statusCode ++ "): " ++ body

        BadResponse { metadata, body } ->
            "Bad response (" ++ String.fromInt metadata.statusCode ++ "): " ++ aToString body

        Timeout ->
            "Timeout"

        NetworkError ->
            "Network error"


errorString_ : ErrorBody a -> String
errorString_ error =
    case error of
        BadUrl url ->
            "Bad url: " ++ url

        UnknownResponse { metadata, body } ->
            "Unknown response (status " ++ String.fromInt metadata.statusCode ++ "): " ++ body

        BadResponse { metadata } ->
            "Bad response (" ++ String.fromInt metadata.statusCode ++ ")"

        Timeout ->
            "Timeout"

        NetworkError ->
            "Network error"


expectMetadataAndString :
    (Http.Metadata -> String -> Result String bad)
    -> (Http.Metadata -> String -> Result String good)
    -> (Result (ErrorBody bad) good -> msg)
    -> Http.Expect msg
expectMetadataAndString parseBad parseGood toMsg =
    Http.expectStringResponse toMsg <|
        \response ->
            case response of
                Http.BadUrl_ url ->
                    Err (BadUrl url)

                Http.Timeout_ ->
                    Err Timeout

                Http.NetworkError_ ->
                    Err NetworkError

                Http.BadStatus_ metadata body ->
                    parseBad metadata body
                        |> Result.mapError (\reason -> UnknownResponse { metadata = metadata, body = body, reason = reason })
                        |> Result.andThen (\parsedBody -> Err (BadResponse { metadata = metadata, body = parsedBody }))

                Http.GoodStatus_ metadata body ->
                    parseGood metadata body
                        |> Result.mapError (\reason -> UnknownResponse { metadata = metadata, body = body, reason = reason })


expectJsonBodies :
    (Result (ErrorBody bad) good -> msg)
    -> (Http.Metadata -> Decoder bad)
    -> (Http.Metadata -> Decoder good)
    -> Http.Expect msg
expectJsonBodies toMsg badDecoder goodDecoder =
    let
        parseString decoder metadata string =
            Decode.decodeString (decoder metadata) string
                |> Result.mapError Decode.errorToString
    in
    expectMetadataAndString (parseString badDecoder) (parseString goodDecoder) toMsg


onStatus : Int -> Decode.Decoder a -> Http.Metadata -> Decode.Decoder a
onStatus expectedStatus decodeBody metadata =
    if metadata.statusCode == expectedStatus then
        decodeBody

    else
        Decode.fail "Unrecognized status code."
