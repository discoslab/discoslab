module Discoslab.Utils.Json exposing
    ( decodeFromString
    , decodeMaybe
    , decodeMaybeAt
    , encodeOrNull
    )

import Json.Decode as Decode
import Json.Decode.Pipeline as Pipeline
import Json.Encode as Encode


encodeOrNull : (a -> Encode.Value) -> Maybe a -> Encode.Value
encodeOrNull encode maybeValue =
    case maybeValue of
        Nothing ->
            Encode.null

        Just value ->
            encode value


decodeFromString : (error -> String) -> (String -> Result error success) -> Decode.Decoder success
decodeFromString errorString fromString =
    let
        parseString string =
            case fromString string of
                Err error ->
                    Decode.fail (errorString error)

                Ok value ->
                    Decode.succeed value
    in
    Decode.andThen parseString Decode.string


decodeMaybe : String -> Decode.Decoder a -> Decode.Decoder (Maybe a -> b) -> Decode.Decoder b
decodeMaybe property decode =
    Pipeline.optional property (Decode.map Just decode) Nothing


decodeMaybeAt : List String -> Decode.Decoder a -> Decode.Decoder (Maybe a -> b) -> Decode.Decoder b
decodeMaybeAt property decode =
    Pipeline.optionalAt property (Decode.map Just decode) Nothing
