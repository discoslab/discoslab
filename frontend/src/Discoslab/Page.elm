module Discoslab.Page exposing
    ( Model(..)
    , Msg(..)
    , init
    , update
    , view
    )

import Browser
import Discoslab.Effect as Effect exposing (Effect)
import Discoslab.Page.Contact as Contact
import Discoslab.Page.Home as Home
import Discoslab.Page.NotFound as NotFound
import Discoslab.Url.Route as Route exposing (Route)
import Html


type Model
    = NotFoundModel
    | HomeModel
    | ContactModel Contact.Model


type Msg
    = ContactMsg Contact.Msg


init : Route -> Model
init route =
    case route of
        Route.NotFound _ ->
            NotFoundModel

        Route.Home ->
            HomeModel

        Route.Contact ->
            ContactModel Contact.init


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        ContactMsg contactMsg ->
            case model of
                ContactModel contactModel ->
                    Contact.update contactMsg contactModel
                        |> Tuple.mapBoth ContactModel (Effect.map ContactMsg)

                _ ->
                    ( model, Effect.None )


view : Model -> Browser.Document Msg
view model =
    let
        mapDocument toMsg document =
            { title = document.title
            , body = List.map (Html.map toMsg) document.body
            }
    in
    case model of
        NotFoundModel ->
            mapDocument identity NotFound.view

        HomeModel ->
            mapDocument identity Home.view

        ContactModel contactModel ->
            mapDocument ContactMsg (Contact.view contactModel)
