module Discoslab.Page.Contact exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Browser
import Discoslab.Contact.EmailAddress as EmailAddress exposing (EmailAddress)
import Discoslab.Contact.Message as Message exposing (Message)
import Discoslab.Contact.Name as Name exposing (Name)
import Discoslab.Contact.Subject as Subject exposing (Subject)
import Discoslab.Effect as Effect exposing (Effect)
import Discoslab.Effect.Contact as Contact
import Discoslab.Effect.Logging as Logging
import Discoslab.Monad.State as State exposing (StateMaybe)
import Discoslab.Ui.Button as Button
import Discoslab.Ui.Fields.Text as TextField
import Discoslab.Ui.Fields.TextArea as TextAreaField
import Discoslab.Ui.Form as Form
import Discoslab.Ui.Text.Heading as Heading
import Discoslab.Ui.Text.Paragraph as Paragraph
import Discoslab.Utils.Http as HttpUtils
import Html exposing (Html)


type alias Model =
    { fromEmailAddress : String
    , fromEmailAddressError : Maybe EmailAddress.Error
    , fromName : String
    , fromNameError : Maybe Name.Error
    , subject : String
    , subjectError : Maybe Subject.Error
    , message : String
    , messageError : Maybe Message.Error
    , isRequestActive : Bool
    , response : Maybe (Result Contact.Error Contact.AcceptedResponse)
    }


type Msg
    = FromEmailAddressChanged String
    | FromEmailAddressBlurred
    | FromNameChanged String
    | FromNameBlurred
    | SubjectChanged String
    | SubjectBlurred
    | MessageChanged String
    | MessageBlurred
    | FormSubmitted
    | ResponseReceived (Result Contact.Error Contact.AcceptedResponse)
    | ResetClicked


init : Model
init =
    { fromEmailAddress = ""
    , fromEmailAddressError = Nothing
    , fromName = ""
    , fromNameError = Nothing
    , subject = ""
    , subjectError = Nothing
    , message = ""
    , messageError = Nothing
    , isRequestActive = False
    , response = Nothing
    }


parseFromEmailAddress : StateMaybe Model (Maybe EmailAddress)
parseFromEmailAddress model =
    case EmailAddress.fromString model.fromEmailAddress of
        Err EmailAddress.Empty ->
            ( { model | fromEmailAddressError = Nothing }
            , Just Nothing
            )

        Err error ->
            ( { model | fromEmailAddressError = Just error }
            , Nothing
            )

        Ok value ->
            ( { model | fromEmailAddressError = Nothing }
            , Just (Just value)
            )


parseFromName : StateMaybe Model (Maybe Name)
parseFromName model =
    case Name.fromString model.fromName of
        Err Name.Empty ->
            ( { model | fromNameError = Nothing }
            , Just Nothing
            )

        Err error ->
            ( { model | fromNameError = Just error }
            , Nothing
            )

        Ok value ->
            ( { model | fromNameError = Nothing }
            , Just (Just value)
            )


parseSubject : StateMaybe Model (Maybe Subject)
parseSubject model =
    case Subject.fromString model.subject of
        Err Subject.Empty ->
            ( { model | subjectError = Nothing }
            , Just Nothing
            )

        Err error ->
            ( { model | subjectError = Just error }
            , Nothing
            )

        Ok value ->
            ( { model | subjectError = Nothing }
            , Just (Just value)
            )


parseMessage : StateMaybe Model Message
parseMessage model =
    case Message.fromString model.message of
        Err error ->
            ( { model | messageError = Just error }
            , Nothing
            )

        Ok value ->
            ( { model | messageError = Nothing }
            , Just value
            )


parseRequest : StateMaybe Model Contact.Request
parseRequest =
    State.mapMaybe Contact.Request parseFromEmailAddress
        |> State.applyMaybe parseFromName
        |> State.applyMaybe parseSubject
        |> State.applyMaybe parseMessage


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        FromEmailAddressChanged value ->
            ( { model | fromEmailAddress = value }
            , Effect.None
            )

        FromEmailAddressBlurred ->
            ( Tuple.first (parseFromEmailAddress model)
            , Effect.None
            )

        FromNameChanged value ->
            ( { model | fromName = value }
            , Effect.None
            )

        FromNameBlurred ->
            ( Tuple.first (parseFromName model)
            , Effect.None
            )

        SubjectChanged value ->
            ( { model | subject = value }
            , Effect.None
            )

        SubjectBlurred ->
            ( Tuple.first (parseSubject model)
            , Effect.None
            )

        MessageChanged value ->
            ( { model | message = value }
            , Effect.None
            )

        MessageBlurred ->
            ( Tuple.first (parseMessage model)
            , Effect.None
            )

        FormSubmitted ->
            if not model.isRequestActive then
                let
                    ( updatedModel, maybeRequest ) =
                        parseRequest model
                in
                case maybeRequest of
                    Nothing ->
                        ( updatedModel, Effect.None )

                    Just request ->
                        ( { updatedModel
                            | isRequestActive = True
                            , response = Nothing
                          }
                        , Effect.Contact
                            { request = request
                            , onResponse = ResponseReceived
                            }
                        )

            else
                ( model, Effect.None )

        ResponseReceived response ->
            ( { model | isRequestActive = False, response = Just response }
            , case response of
                Ok _ ->
                    Effect.None

                Err error ->
                    Effect.Logging
                        { severity = Logging.Error
                        , message = HttpUtils.errorString_ error
                        , namespace = [ "contact" ]
                        , context = Nothing
                        }
            )

        ResetClicked ->
            ( init, Effect.None )


viewResponse : Maybe (Result Contact.Error Contact.AcceptedResponse) -> Html msg
viewResponse response =
    case response of
        Nothing ->
            Html.text ""

        Just (Err _) ->
            Paragraph.view
                [ Html.text "Something went wrong. Failed to send to your message."
                ]

        Just (Ok _) ->
            Paragraph.view
                [ Html.text "Your message was succesfully sent!"
                ]


view : Model -> Browser.Document Msg
view model =
    { title = "Contact"
    , body =
        [ Heading.config "Send me a message"
            |> Heading.setLevel Heading.High
            |> Heading.setAlignment Heading.Center
            |> Heading.view
        , Form.config "contact"
            |> Form.setOnSubmit FormSubmitted
            |> Form.view
                [ TextField.config { id = "from-email", label = "Email" }
                    |> TextField.setOnInput FromEmailAddressChanged
                    |> TextField.setOnBlur FromEmailAddressBlurred
                    |> TextField.setValue model.fromEmailAddress
                    |> TextField.setError (Maybe.map EmailAddress.errorString model.fromEmailAddressError)
                    |> TextField.view
                , TextField.config { id = "from-name", label = "Name" }
                    |> TextField.setOnInput FromNameChanged
                    |> TextField.setOnBlur FromNameBlurred
                    |> TextField.setValue model.fromName
                    |> TextField.setError (Maybe.map Name.errorString model.fromNameError)
                    |> TextField.view
                , TextField.config { id = "subject", label = "Subject" }
                    |> TextField.setOnInput SubjectChanged
                    |> TextField.setOnBlur SubjectBlurred
                    |> TextField.setValue model.subject
                    |> TextField.setError (Maybe.map Subject.errorString model.subjectError)
                    |> TextField.view
                , TextAreaField.config { id = "message", label = "Message" }
                    |> TextAreaField.setOnInput MessageChanged
                    |> TextAreaField.setOnBlur MessageBlurred
                    |> TextAreaField.setValue model.message
                    |> TextAreaField.setError (Maybe.map Message.errorString model.messageError)
                    |> TextAreaField.setIsRequired True
                    |> TextAreaField.view
                , viewResponse model.response
                , Button.config { id = "submit", label = "Send" }
                    |> Button.setType Button.Submit
                    |> Button.setRelation Button.Primary
                    |> Button.setIsBusy model.isRequestActive
                    |> Button.view
                , Button.config { id = "reset", label = "Reset" }
                    |> Button.setType Button.Reset
                    |> Button.setRelation Button.Dangerous
                    |> Button.setOnClick ResetClicked
                    |> Button.view
                ]
        ]
    }
