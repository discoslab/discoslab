module Discoslab.Page.Home exposing (view)

import Browser
import Discoslab.Ui.Text.Heading as Heading
import Discoslab.Ui.Text.Link as Link
import Discoslab.Ui.Text.Paragraph as Paragraph
import Html


view : Browser.Document msg
view =
    { title = ""
    , body =
        [ Heading.config "Welcome to Disco's Lab"
            |> Heading.setLevel Heading.High
            |> Heading.setAlignment Heading.Center
            |> Heading.view
        , Paragraph.view
            [ Html.text "Hello, welcome to my website! I’m a full stack engineer who is enthusiastic about static types, functional programming, and FOSS software."
            ]
        , Paragraph.view
            [ Link.config "https://gitlab.com/discoslab/discoslab"
                |> Link.setDisplayText "Source code of this website"
                |> Link.view
            ]
        , Paragraph.view
            [ Link.config "https://gitlab.com/disco-dave"
                |> Link.setDisplayText "My gitlab profile"
                |> Link.view
            ]
        , Paragraph.view
            [ Link.config "https://github.com/disco-dave"
                |> Link.setDisplayText "My github profile"
                |> Link.view
            ]
        ]
    }
