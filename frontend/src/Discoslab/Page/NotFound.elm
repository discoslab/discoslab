module Discoslab.Page.NotFound exposing (view)

import Browser
import Discoslab.Ui.Text.Heading as Heading


view : Browser.Document msg
view =
    { title = "Not Found"
    , body =
        [ Heading.config "This page cannot be found."
            |> Heading.setLevel Heading.High
            |> Heading.setAlignment Heading.Center
            |> Heading.view
        ]
    }
