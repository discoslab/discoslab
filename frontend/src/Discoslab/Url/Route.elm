module Discoslab.Url.Route exposing
    ( Route(..)
    , parse
    , toString
    )

import Discoslab.Url.BasePath as BasePath exposing (BasePath)
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser)


type Route
    = NotFound Url
    | Home
    | Contact


contact : String
contact =
    "contact"


routeParser : Parser (Route -> a) a
routeParser =
    Parser.oneOf
        [ Parser.map Home Parser.top
        , Parser.map Contact (Parser.s contact)
        ]


toString : BasePath -> Route -> String
toString basePath route =
    case route of
        Home ->
            BasePath.absolute basePath [] []

        Contact ->
            BasePath.absolute basePath [ contact ] []

        NotFound url ->
            Url.toString url


parse : BasePath -> Url -> Route
parse basePath url =
    let
        parser =
            if BasePath.isEmpty basePath then
                routeParser

            else
                BasePath.path basePath </> routeParser

        maybePath =
            Parser.parse parser url
    in
    Maybe.withDefault (NotFound url) maybePath
