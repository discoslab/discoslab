module Discoslab.Url.BasePath exposing
    ( BasePath
    , absolute
    , decode
    , encode
    , fromString
    , isEmpty
    , path
    )

import Json.Decode as Decode
import Json.Encode as Encode
import Url.Builder as UrlBuilder exposing (QueryParameter)
import Url.Parser as UrlParser exposing ((</>), Parser)


type BasePath
    = BasePath (List String)


fromString : String -> BasePath
fromString string =
    String.trim string
        |> String.split "/"
        |> List.filter (not << String.isEmpty)
        |> BasePath


absolute : BasePath -> List String -> List QueryParameter -> String
absolute (BasePath basePaths) paths queryParams =
    UrlBuilder.absolute (basePaths ++ paths) queryParams


path : BasePath -> Parser a a
path (BasePath paths) =
    let
        combine r l =
            l </> UrlParser.s r
    in
    List.foldl combine UrlParser.top paths


isEmpty : BasePath -> Bool
isEmpty (BasePath paths) =
    List.isEmpty paths


encode : BasePath -> Encode.Value
encode basePath =
    Encode.string (absolute basePath [] [])


decode : Decode.Decoder BasePath
decode =
    Decode.map fromString Decode.string
