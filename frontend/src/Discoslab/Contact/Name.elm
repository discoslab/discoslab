module Discoslab.Contact.Name exposing
    ( Error(..)
    , Name
    , decode
    , encode
    , errorString
    , fromString
    , toString
    )

import Discoslab.Utils.Json as JsonUtils
import Json.Decode as Decode
import Json.Encode as Encode


type Name
    = Name String


type Error
    = Empty
    | LongerThan100Characters


errorString : Error -> String
errorString error =
    case error of
        Empty ->
            "Name cannot be empty."

        LongerThan100Characters ->
            "Name cannot be longer than 100 characters."


fromString : String -> Result Error Name
fromString string =
    let
        trimmedString =
            String.trim string
    in
    if String.isEmpty trimmedString then
        Err Empty

    else if String.length trimmedString > 100 then
        Err LongerThan100Characters

    else
        Ok (Name trimmedString)


toString : Name -> String
toString (Name value) =
    value


encode : Name -> Encode.Value
encode =
    Encode.string << toString


decode : Decode.Decoder Name
decode =
    JsonUtils.decodeFromString errorString fromString
