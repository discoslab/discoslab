module Discoslab.Contact.Subject exposing
    ( Error(..)
    , Subject
    , decode
    , encode
    , errorString
    , fromString
    , toString
    )

import Discoslab.Utils.Json as JsonUtils
import Json.Decode as Decode
import Json.Encode as Encode


type Subject
    = Subject String


type Error
    = Empty
    | LongerThan100Characters


errorString : Error -> String
errorString error =
    case error of
        Empty ->
            "Subject cannot be empty."

        LongerThan100Characters ->
            "Subject cannot be longer than 100 characters."


fromString : String -> Result Error Subject
fromString string =
    let
        trimmedString =
            String.trim string
    in
    if String.isEmpty trimmedString then
        Err Empty

    else if String.length trimmedString > 100 then
        Err LongerThan100Characters

    else
        Ok (Subject trimmedString)


toString : Subject -> String
toString (Subject value) =
    value


encode : Subject -> Encode.Value
encode =
    Encode.string << toString


decode : Decode.Decoder Subject
decode =
    JsonUtils.decodeFromString errorString fromString
