module Discoslab.Contact.Message exposing
    ( Error(..)
    , Message
    , decode
    , encode
    , errorString
    , fromString
    , toString
    )

import Discoslab.Utils.Json as JsonUtils
import Json.Decode as Decode
import Json.Encode as Encode


type Message
    = Message String


type Error
    = Empty
    | LongerThan1000Characters


errorString : Error -> String
errorString error =
    case error of
        Empty ->
            "Message cannot be empty."

        LongerThan1000Characters ->
            "Message cannot be longer than 100 characters."


fromString : String -> Result Error Message
fromString string =
    let
        trimmedString =
            String.trim string
    in
    if String.isEmpty trimmedString then
        Err Empty

    else if String.length trimmedString > 1000 then
        Err LongerThan1000Characters

    else
        Ok (Message trimmedString)


toString : Message -> String
toString (Message value) =
    value


encode : Message -> Encode.Value
encode =
    Encode.string << toString


decode : Decode.Decoder Message
decode =
    JsonUtils.decodeFromString errorString fromString
