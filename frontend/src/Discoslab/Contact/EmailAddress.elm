module Discoslab.Contact.EmailAddress exposing
    ( EmailAddress
    , Error(..)
    , decode
    , encode
    , errorString
    , fromString
    , toString
    )

import Discoslab.Utils.Json as JsonUtils
import Json.Decode as Decode
import Json.Encode as Encode
import Regex exposing (Regex)


type EmailAddress
    = EmailAddress String


type Error
    = Empty
    | Invalid


errorString : Error -> String
errorString error =
    case error of
        Empty ->
            "Email cannot be empty."

        Invalid ->
            "Email address is invalid."


validEmail : Regex
validEmail =
    "^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        |> Regex.fromStringWith { caseInsensitive = True, multiline = False }
        |> Maybe.withDefault Regex.never


fromString : String -> Result Error EmailAddress
fromString string =
    let
        trimmedString =
            String.trim string
    in
    if String.isEmpty trimmedString then
        Err Empty

    else if Regex.contains validEmail trimmedString then
        Ok (EmailAddress trimmedString)

    else
        Err Invalid


toString : EmailAddress -> String
toString (EmailAddress value) =
    value


encode : EmailAddress -> Encode.Value
encode =
    Encode.string << toString


decode : Decode.Decoder EmailAddress
decode =
    JsonUtils.decodeFromString errorString fromString
