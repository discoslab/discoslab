module Discoslab.Ui.Loading exposing (ellipsis)

import Html exposing (Html)
import Html.Attributes as Attrs


ellipsis : Html msg
ellipsis =
    Html.div [ Attrs.class "loading-ellipsis" ]
        [ Html.div [ Attrs.class "loading-ellipsis__dot" ] []
        , Html.div [ Attrs.class "loading-ellipsis__dot" ] []
        , Html.div [ Attrs.class "loading-ellipsis__dot" ] []
        , Html.div [ Attrs.class "loading-ellipsis__dot" ] []
        ]
