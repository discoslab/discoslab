module Discoslab.Ui.Nav exposing
    ( Link
    , view
    )

import Discoslab.Url.BasePath exposing (BasePath)
import Discoslab.Url.Route as Route exposing (Route)
import Html exposing (Html)
import Html.Attributes as Attrs


type alias Link =
    { name : String
    , route : Route
    , isActive : Bool
    }


viewLink : BasePath -> Link -> Html msg
viewLink basePath link =
    Html.li [ Attrs.class "nav__item" ]
        [ Html.a
            [ Attrs.class "nav__link"
            , Attrs.classList [ ( "nav__link--active", link.isActive ) ]
            , Attrs.href (Route.toString basePath link.route)
            ]
            [ Html.text link.name ]
        ]


view : BasePath -> List Link -> Html msg
view basePath links =
    Html.nav []
        [ Html.ul [ Attrs.class "nav" ] (List.map (viewLink basePath) links)
        ]
