module Discoslab.Ui.Text.Link exposing
    ( Config
    , config
    , setDisplayText
    , setIsActive
    , view
    )

import Html exposing (Html)
import Html.Attributes as Attrs


type Config
    = Config
        { href : String
        , displayText : Maybe String
        , isActive : Bool
        }


config : String -> Config
config href =
    Config
        { href = href
        , displayText = Nothing
        , isActive = False
        }


setDisplayText : String -> Config -> Config
setDisplayText displayText (Config conf) =
    Config { conf | displayText = Just displayText }


setIsActive : Bool -> Config -> Config
setIsActive isActive (Config conf) =
    Config { conf | isActive = isActive }


view : Config -> Html msg
view (Config conf) =
    let
        linkText =
            Maybe.withDefault conf.href conf.displayText
    in
    Html.a
        [ Attrs.class "link"
        , Attrs.classList [ ( "link--active", conf.isActive ) ]
        , Attrs.href conf.href
        ]
        [ Html.text linkText ]
