module Discoslab.Ui.Text.Paragraph exposing (view)

import Html exposing (Html)
import Html.Attributes as Attrs


view : List (Html msg) -> Html msg
view children =
    Html.p [ Attrs.class "paragraph" ] children
