module Discoslab.Ui.Fields.Shared exposing
    ( Config
    , view
    )

import Html exposing (Html)
import Html.Attributes as Attrs
import Maybe.Extra exposing (isJust)


type alias Config =
    { id : String
    , label : String
    , isRequired : Bool
    , isDisabled : Bool
    , error : Maybe String
    , help : Maybe String
    }


view : Config -> Html msg -> Html msg
view conf input =
    Html.div
        [ Attrs.class "field"
        , Attrs.classList
            [ ( "field--invalid", isJust conf.error )
            , ( "field--required", conf.isRequired )
            , ( "field--disabled", conf.isDisabled )
            ]
        ]
        [ Html.label
            [ Attrs.class "field__label"
            , Attrs.for conf.id
            ]
            [ Html.text conf.label
            , if conf.isRequired then
                Html.text ""

              else
                Html.span
                    [ Attrs.class "field__optional" ]
                    [ Html.text "optional" ]
            ]
        , case conf.help of
            Nothing ->
                Html.text ""

            Just help ->
                Html.p
                    [ Attrs.class "field__help" ]
                    [ Html.text help ]
        , input
        , Html.p
            [ Attrs.class "field__error" ]
            [ Html.text (Maybe.withDefault "" conf.error) ]
        ]
