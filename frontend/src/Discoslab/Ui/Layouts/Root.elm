module Discoslab.Ui.Layouts.Root exposing (view)

import Discoslab.Ui.Nav as Nav
import Discoslab.Url.BasePath exposing (BasePath)
import Discoslab.Url.Route as Route exposing (Route)
import Html exposing (Html)
import Html.Attributes as Attrs


view : BasePath -> Route -> List (Html msg) -> List (Html msg)
view basePath currentRoute children =
    [ Html.div [ Attrs.class "root" ]
        [ Html.header [ Attrs.class "root__header" ]
            [ Nav.view basePath <|
                let
                    toItem name route =
                        { name = name
                        , route = route
                        , isActive = currentRoute == route
                        }
                in
                [ toItem "Home" Route.Home
                , toItem "Contact" Route.Contact
                ]
            ]
        , Html.main_ [ Attrs.class "root__main" ] children
        ]
    ]
