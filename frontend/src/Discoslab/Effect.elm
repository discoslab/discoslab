module Discoslab.Effect exposing
    ( Dependencies
    , Effect(..)
    , map
    , toCmd
    , toList
    )

import Browser.Navigation
import Discoslab.Effect.Contact as Contact
import Discoslab.Effect.Logging as Logging
import Discoslab.Effect.Navigation as Navigation
import Discoslab.Url.BasePath exposing (BasePath)


type alias Dependencies msg =
    { api : BasePath
    , namespace : List String
    , minSeverity : Logging.Severity
    , noOp : msg
    , navigationKey : Browser.Navigation.Key
    }


type Effect msg
    = None
    | Batch (List (Effect msg))
    | Logging Logging.Effect
    | Navigation Navigation.Effect
    | Contact (Contact.Effect msg)


map : (a -> b) -> Effect a -> Effect b
map f effect =
    case effect of
        None ->
            None

        Batch effects ->
            Batch (List.map (map f) effects)

        Logging e ->
            Logging e

        Navigation e ->
            Navigation e

        Contact e ->
            Contact (Contact.map f e)


toList : Effect msg -> List (Effect msg)
toList effect =
    case effect of
        None ->
            []

        Batch effects ->
            List.concatMap toList effects

        e ->
            [ e ]


toCmd : Dependencies msg -> Effect msg -> Cmd msg
toCmd deps effect =
    case effect of
        None ->
            Cmd.none

        Batch batchedEffects ->
            batchedEffects
                |> List.map (toCmd deps)
                |> Cmd.batch

        Logging e ->
            Logging.toCmd deps e

        Navigation e ->
            Navigation.toCmd deps e

        Contact e ->
            Contact.toCmd deps e
