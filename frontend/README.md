# Disco's Lab Web Frontend

The web frontend for [https://discoslab.com](https://discoslab.com). 
Built using Elm, JavaScript, and CSS.

## Getting started


### Setup
1. Set environment variables. See [../.envrc.sample](../.envrc.sample).
2. `npm install` - Install all dependencies

---

### Production bundle
```
npm run build
```
Generates a production bundle in the `./dist` directory.

You can use the `DISCOSLAB_FRONTEND_BASE_PATH` environment variable to set the base path for all of the routes and links.

---

### Development
All of the source code can be found in the [./src](./src) directory.

```
npm run serve
```
This starts a development server at [http://localhost:3000](http://localhost:3000), 
complete with hot-module reloading for Elm, JavaScript, and CSS.

The port of this server can be configured with the `DISCOSLAB_FRONTEND_PORT` environment variable.

You will also need to start the [webapi](../webapi) so the `frontend` has a REST API to talk to.
The API will be proxied and available to the `frontend` as `/api` or [http://localhost:3000/api](http://localhost:3000/api).

---

```
npm run repl
```
Starts an Elm REPL, allowing you to debug the Elm application.

---

### Testing
All of the tests can be found in the [./tests](./tests) directory.

```
npm run test
```
This runs all of the tests.

---

```
npm run test:coverage
```
Generates a test coverage report in the `./.coverage` directory.

---

### Linting
```
npm run lint
```
Runs all of the linters printing all of the warnings and errors, but does not fix anything.

---

```
npm run lint:fix
```
Runs all of the linters printing all of the warnings and errors, but attempts to fix violations.

---

```
npm run lint:all
```
Sames `npm run lint`, accept it ignores all special comments and files the exclude sections from being linted.

---

All of the above commands can be restricted to only Elm, JavaScript, or CSS. This is done by appending `:elm`, `:js`, or `:css` to the end the of name.
```
npm run lint:elm
npm run lint:js
npm run lint:css
```

```
npm run lint:fix:elm
npm run lint:fix:js
npm run lint:fix:css
```

```
npm run lint:all:elm
npm run lint:all:js
npm run lint:all:css
```

---

#### Configuration

[./review](./review) - Elm

[./.eslintrc.json](./.eslintrc.json) - JavaScript

[./.stylelintrc.json](./.stylelintrc.json) - CSS

---

### Formatting
```
npm run format
```
Checks that all files are formatted.

---

```
npm run format:fix
```
Formats all files.

---

All of the above commands can be restricted to only Elm, JavaScript, or CSS. This is done by appending `:elm`, `:js`, or `:css` to the end of the name.
```
npm run format:elm
npm run format:js
npm run format:css
```

```
npm run format:fix:elm
npm run format:fix:js
npm run format:fix:css
```

---

#### Configuration

There is currently no configuration for any formatter. Instead they are all using the defaults.
