module Discoslab.Monad.StateTests exposing (tests)

import Dict
import Discoslab.Monad.State as State
import Discoslab.Test.LessThanTen as LessThanTen
import Discoslab.Test.Model as Model
import Discoslab.Test.MultipleOfThree as MultipleOfThree
import Discoslab.Test.OneWord as OneWord
import Expect
import Test exposing (Test)


tests : Test
tests =
    Test.describe "Discoslab.Monad.State"
        [ Test.describe "State"
            [ Test.test "run returns both the state and value" <|
                \() ->
                    let
                        initialState =
                            123

                        computation state =
                            ( state * 2, "abc" )

                        expected =
                            ( 246, "abc" )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            , Test.test "map allows you to change the value inside" <|
                \() ->
                    let
                        initialState =
                            123

                        computation =
                            State.map String.length (\state -> ( state * 2, "abc" ))

                        expected =
                            ( 246, 3 )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            , Test.test "pure wraps any value without touching the state" <|
                \() ->
                    let
                        initialState =
                            123

                        computation =
                            State.pure "abc"

                        expected =
                            ( 123, "abc" )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            , Test.test "apply doesn't discard any state" <|
                \() ->
                    let
                        initialState =
                            []

                        computationA state =
                            ( "a" :: state, 12 )

                        computationB state =
                            ( "b" :: state, 30 )

                        computation =
                            State.map (+) computationA
                                |> State.apply computationB

                        expected =
                            ( [ "b", "a" ], 42 )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            , Test.test "traverse doesn't discard any state" <|
                \() ->
                    let
                        initialState =
                            []

                        double input state =
                            ( String.fromInt input :: state, input * 2 )

                        computation =
                            State.traverse double [ 1, 2, 3 ]

                        expected =
                            ( [ "3", "2", "1" ], [ 2, 4, 6 ] )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            , Test.test "traverseDict doesn't discard any state" <|
                \() ->
                    let
                        initialState =
                            []

                        double input state =
                            ( String.fromInt input :: state, input * 2 )

                        computation =
                            State.traverseDict double (Dict.fromList [ ( "a", 1 ), ( "b", 2 ), ( "c", 3 ) ])

                        expected =
                            ( [ "3", "2", "1" ], Dict.fromList [ ( "a", 2 ), ( "b", 4 ), ( "c", 6 ) ] )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            ]
        , Test.describe "StateMaybe"
            [ Test.test "accumulates all errors" <|
                \() ->
                    let
                        initialState =
                            { oneWord = "abc efg"
                            , oneWordError = Nothing
                            , lessThanTen = 20
                            , lessThanTenError = Nothing
                            , multipleOfThree = 4
                            , multipleOfThreeError = Nothing
                            }

                        expectedState =
                            { oneWord = "abc efg"
                            , oneWordError = Just OneWord.errorMessage
                            , lessThanTen = 20
                            , lessThanTenError = Just LessThanTen.errorMessage
                            , multipleOfThree = 4
                            , multipleOfThreeError = Just MultipleOfThree.errorMessage
                            }

                        actual =
                            Model.parseModel initialState
                    in
                    Expect.equal actual ( expectedState, Nothing )
            , Test.test "accumulates all errors even when one field succeeds" <|
                \() ->
                    let
                        initialState =
                            { oneWord = "abc efg"
                            , oneWordError = Nothing
                            , lessThanTen = 3
                            , lessThanTenError = Nothing
                            , multipleOfThree = 4
                            , multipleOfThreeError = Nothing
                            }

                        expectedState =
                            { oneWord = "abc efg"
                            , oneWordError = Just OneWord.errorMessage
                            , lessThanTen = 3
                            , lessThanTenError = Nothing
                            , multipleOfThree = 4
                            , multipleOfThreeError = Just MultipleOfThree.errorMessage
                            }

                        actual =
                            Model.parseModel initialState
                    in
                    Expect.equal actual ( expectedState, Nothing )
            , Test.test "still updates state when everything succeeds" <|
                \() ->
                    let
                        initialState =
                            { oneWord = "abc"
                            , oneWordError = Just OneWord.errorMessage
                            , lessThanTen = 3
                            , lessThanTenError = Just LessThanTen.errorMessage
                            , multipleOfThree = 9
                            , multipleOfThreeError = Just MultipleOfThree.errorMessage
                            }

                        expectedState =
                            { oneWord = "abc"
                            , oneWordError = Nothing
                            , lessThanTen = 3
                            , lessThanTenError = Nothing
                            , multipleOfThree = 9
                            , multipleOfThreeError = Nothing
                            }

                        expectedParsedModel =
                            { oneWord = Just "abc"
                            , lessThanTen = Just 3
                            , multipleOfThree = Just 9
                            }

                        ( actualState, actualParsedModel ) =
                            Model.parseModel initialState

                        actual =
                            ( actualState
                            , Just
                                { oneWord = Maybe.map (OneWord.toString << .oneWord) actualParsedModel
                                , lessThanTen = Maybe.map (LessThanTen.toInt << .lessThanTen) actualParsedModel
                                , multipleOfThree = Maybe.map (MultipleOfThree.toInt << .multipleOfThree) actualParsedModel
                                }
                            )
                    in
                    Expect.equal actual ( expectedState, Just expectedParsedModel )
            , Test.test "traverseMaybe doesn't discard any state when the function returns Nothing" <|
                \() ->
                    let
                        initialState =
                            []

                        double input state =
                            ( String.fromInt input :: state
                            , if input < 10 then
                                Just (input * 2)

                              else
                                Nothing
                            )

                        computation =
                            State.traverseMaybe double [ 1, 2, 11, 3 ]

                        expected =
                            ( [ "3", "11", "2", "1" ], Nothing )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            , Test.test "traverseMaybe doesn't discard any state when the function returns only Just" <|
                \() ->
                    let
                        initialState =
                            []

                        double input state =
                            ( String.fromInt input :: state
                            , if input < 10 then
                                Just (input * 2)

                              else
                                Nothing
                            )

                        computation =
                            State.traverseMaybe double [ 1, 2, 5, 3 ]

                        expected =
                            ( [ "3", "5", "2", "1" ], Just [ 2, 4, 10, 6 ] )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            , Test.test "traverseDictMaybe doesn't discard any state when the function returns Nothing" <|
                \() ->
                    let
                        initialState =
                            []

                        double input state =
                            ( String.fromInt input :: state
                            , if input < 10 then
                                Just (input * 2)

                              else
                                Nothing
                            )

                        computation =
                            State.traverseDictMaybe double (Dict.fromList [ ( "a", 1 ), ( "b", 2 ), ( "c", 11 ), ( "d", 3 ) ])

                        expected =
                            ( [ "3", "11", "2", "1" ], Nothing )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            , Test.test "traverseDictMaybe doesn't discard any state when the function returns only Just" <|
                \() ->
                    let
                        initialState =
                            []

                        double input state =
                            ( String.fromInt input :: state
                            , if input < 10 then
                                Just (input * 2)

                              else
                                Nothing
                            )

                        computation =
                            State.traverseDictMaybe double (Dict.fromList [ ( "a", 1 ), ( "b", 2 ), ( "c", 5 ), ( "d", 3 ) ])

                        expected =
                            ( [ "3", "5", "2", "1" ], Just (Dict.fromList [ ( "a", 2 ), ( "b", 4 ), ( "c", 10 ), ( "d", 6 ) ]) )

                        actual =
                            State.run initialState computation
                    in
                    Expect.equal expected actual
            ]
        ]
