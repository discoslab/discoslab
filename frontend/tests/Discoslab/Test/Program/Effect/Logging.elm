module Discoslab.Test.Program.Effect.Logging exposing (simulate)

import Discoslab.Effect.Logging as Logging
import ProgramTest exposing (SimulatedEffect)
import SimulatedEffect.Ports as SimulatedPorts


simulate : Logging.Effect -> SimulatedEffect msg
simulate effect =
    SimulatedPorts.send "log" (Logging.encode effect)
