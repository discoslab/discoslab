module Discoslab.Test.Program.Effect.Contact exposing (simulate)

import Discoslab.Effect.Contact as Contact
import ProgramTest exposing (SimulatedEffect)
import SimulatedEffect.Cmd as SimulatedCmd


simulate : Contact.Effect msg -> SimulatedEffect msg
simulate _ =
    SimulatedCmd.none
