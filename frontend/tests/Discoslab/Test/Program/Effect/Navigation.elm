module Discoslab.Test.Program.Effect.Navigation exposing (simulate)

import Discoslab.Effect.Navigation as Navigation
import ProgramTest exposing (SimulatedEffect)
import SimulatedEffect.Cmd as SimulatedCmd
import SimulatedEffect.Navigation as SimulatedNavigation


simulate : Navigation.Effect -> SimulatedEffect msg
simulate effect =
    case effect of
        Navigation.PushUrl url ->
            SimulatedNavigation.pushUrl url

        Navigation.ReplaceUrl url ->
            SimulatedNavigation.replaceUrl url

        Navigation.Back n ->
            SimulatedNavigation.back n

        Navigation.Forward _ ->
            SimulatedCmd.none

        Navigation.Load url ->
            SimulatedNavigation.load url

        Navigation.Reload ->
            SimulatedNavigation.reload

        Navigation.ReloadAndSkipCache ->
            SimulatedNavigation.reloadAndSkipCache
