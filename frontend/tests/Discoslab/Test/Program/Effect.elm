module Discoslab.Test.Program.Effect exposing (simulate)

import Discoslab.Effect as Effect exposing (Effect)
import Discoslab.Test.Program.Effect.Contact as Contact
import Discoslab.Test.Program.Effect.Logging as Logging
import Discoslab.Test.Program.Effect.Navigation as Navigation
import ProgramTest exposing (SimulatedEffect)
import SimulatedEffect.Cmd as SimulatedCmd


simulate : Effect msg -> SimulatedEffect msg
simulate effect =
    case effect of
        Effect.None ->
            SimulatedCmd.none

        Effect.Batch batchedEffects ->
            List.map simulate batchedEffects
                |> SimulatedCmd.batch

        Effect.Navigation e ->
            Navigation.simulate e

        Effect.Logging e ->
            Logging.simulate e

        Effect.Contact e ->
            Contact.simulate e
