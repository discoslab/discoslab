module Discoslab.Test.Program exposing (start)

import Discoslab
import Discoslab.Effect as Effect exposing (Effect)
import Discoslab.Test.Program.Effect as Effect
import Discoslab.Url.BasePath as BasePath
import ProgramTest exposing (ProgramTest)


start : ProgramTest Discoslab.Model Discoslab.Msg (Effect Discoslab.Msg)
start =
    ProgramTest.createApplication
        { init = \_ url _ -> ( Discoslab.init (BasePath.fromString "") url, Effect.None )
        , view = Discoslab.view
        , update = Discoslab.update
        , onUrlRequest = Discoslab.UrlRequested
        , onUrlChange = Discoslab.UrlChanged
        }
        |> ProgramTest.withBaseUrl "https://localhost"
        |> ProgramTest.withSimulatedEffects Effect.simulate
        |> ProgramTest.start ()
