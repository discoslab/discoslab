module Discoslab.Test.MultipleOfThree exposing
    ( MultipleOfThree
    , errorMessage
    , fromInt
    , toInt
    )


type MultipleOfThree
    = MultipleOfThree Int


fromInt : Int -> Maybe MultipleOfThree
fromInt n =
    if modBy 3 n == 0 then
        Just (MultipleOfThree n)

    else
        Nothing


toInt : MultipleOfThree -> Int
toInt (MultipleOfThree n) =
    n


errorMessage : String
errorMessage =
    "Not a multiple of three."
