module Discoslab.Test.Model exposing
    ( Model
    , ParsedModel
    , parseLessThanTen
    , parseModel
    , parseMultipleOfThree
    , parseOneWord
    )

import Discoslab.Monad.State as State
import Discoslab.Test.LessThanTen as LessThanTen exposing (LessThanTen)
import Discoslab.Test.MultipleOfThree as MultipleOfThree exposing (MultipleOfThree)
import Discoslab.Test.OneWord as OneWord exposing (OneWord)


type alias Model =
    { oneWord : String
    , oneWordError : Maybe String
    , lessThanTen : Int
    , lessThanTenError : Maybe String
    , multipleOfThree : Int
    , multipleOfThreeError : Maybe String
    }


parseOneWord : Model -> ( Model, Maybe OneWord )
parseOneWord model =
    case OneWord.fromString model.oneWord of
        Just value ->
            ( { model | oneWordError = Nothing }
            , Just value
            )

        Nothing ->
            ( { model | oneWordError = Just OneWord.errorMessage }
            , Nothing
            )


parseLessThanTen : Model -> ( Model, Maybe LessThanTen )
parseLessThanTen model =
    case LessThanTen.fromInt model.lessThanTen of
        Just value ->
            ( { model | lessThanTenError = Nothing }
            , Just value
            )

        Nothing ->
            ( { model | lessThanTenError = Just LessThanTen.errorMessage }
            , Nothing
            )


parseMultipleOfThree : Model -> ( Model, Maybe MultipleOfThree )
parseMultipleOfThree model =
    case MultipleOfThree.fromInt model.multipleOfThree of
        Just value ->
            ( { model | multipleOfThreeError = Nothing }
            , Just value
            )

        Nothing ->
            ( { model | multipleOfThreeError = Just MultipleOfThree.errorMessage }
            , Nothing
            )


type alias ParsedModel =
    { oneWord : OneWord
    , lessThanTen : LessThanTen
    , multipleOfThree : MultipleOfThree
    }


parseModel : Model -> ( Model, Maybe ParsedModel )
parseModel =
    State.mapMaybe ParsedModel parseOneWord
        |> State.applyMaybe parseLessThanTen
        |> State.applyMaybe parseMultipleOfThree
