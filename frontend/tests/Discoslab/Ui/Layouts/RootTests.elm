module Discoslab.Ui.Layouts.RootTests exposing (tests)

import Discoslab.Ui.Layouts.Root as RootLayout
import Discoslab.Url.BasePath as BasePath
import Discoslab.Url.Route as Route
import Expect exposing (Expectation)
import Html
import Html.Attributes as Attrs
import Test exposing (Test)
import Test.Html.Query as Query
import Test.Html.Selector as Selector


expectSingleNode : List a -> (a -> Expectation) -> Expectation
expectSingleNode layout expect =
    Expect.all
        [ \l -> Expect.equal (List.length l) 1
        , \l ->
            List.head l
                |> Maybe.map expect
                |> Maybe.withDefault (Expect.fail "No HTML was returned.")
        ]
        layout


tests : Test
tests =
    Test.describe "Discoslab.Ui.Layouts.Root"
        [ Test.test "renders everything inside of a single div" <|
            \() ->
                expectSingleNode (RootLayout.view (BasePath.fromString "") Route.Home []) <|
                    \html ->
                        Expect.all
                            [ \_ ->
                                Query.fromHtml html
                                    |> Query.has
                                        [ Selector.tag "div"
                                        , Selector.class "root"
                                        ]
                            , \_ ->
                                Query.fromHtml html
                                    |> Query.children []
                                    |> Query.count (Expect.equal 2)
                            ]
                            ()
        , Test.test "renders a header" <|
            \() ->
                expectSingleNode (RootLayout.view (BasePath.fromString "") Route.Home []) <|
                    \html ->
                        Expect.all
                            [ \_ ->
                                Query.fromHtml html
                                    |> Query.find
                                        [ Selector.tag "header"
                                        ]
                                    |> Query.has
                                        [ Selector.class "root__header"
                                        ]
                            ]
                            ()
        , Test.test "renders a main where it places children" <|
            \() ->
                expectSingleNode (RootLayout.view (BasePath.fromString "") Route.Home [ Html.p [] [ Html.text "sample text" ] ]) <|
                    \html ->
                        Expect.all
                            [ \_ ->
                                Query.fromHtml html
                                    |> Query.find
                                        [ Selector.tag "main"
                                        , Selector.class "root__main"
                                        ]
                                    |> Query.find
                                        [ Selector.tag "p"
                                        ]
                                    |> Query.has
                                        [ Selector.exactText "sample text"
                                        ]
                            ]
                            ()
        ]
