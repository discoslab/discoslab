module Discoslab.Ui.Fields.TextTests exposing (tests)

import Discoslab.Ui.Fields.SharedTests as SharedTests
import Discoslab.Ui.Fields.Text as TextField
import Html.Attributes as Attrs
import Test exposing (Test)
import Test.Html.Event as Event
import Test.Html.Query as Query
import Test.Html.Selector as Selector


tests : Test
tests =
    Test.describe "Discoslab.Ui.Fields.Text"
        [ SharedTests.tests <|
            \config ->
                TextField.config
                    { id = config.id
                    , label = config.label
                    }
                    |> TextField.setIsRequired config.isRequired
                    |> TextField.setIsDisabled config.isDisabled
                    |> TextField.setHelp config.help
                    |> TextField.setError config.error
                    |> TextField.view
        , Test.test "input has the correct id, classes, and type" <|
            \() ->
                TextField.config
                    { id = "text-id"
                    , label = "My example text field"
                    }
                    |> TextField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "input" ]
                    |> Query.has
                        [ Selector.id "text-id"
                        , Selector.class "field__input"
                        , Selector.class "field__input--text"
                        , Selector.attribute (Attrs.type_ "text")
                        ]
        , Test.test "input can have a value" <|
            \() ->
                TextField.config
                    { id = "text-id"
                    , label = "My example text field"
                    }
                    |> TextField.setValue "some example value"
                    |> TextField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "input" ]
                    |> Query.has
                        [ Selector.attribute (Attrs.value "some example value")
                        ]
        , Test.test "input can be disabled" <|
            \() ->
                TextField.config
                    { id = "text-id"
                    , label = "My example text field"
                    }
                    |> TextField.setIsDisabled True
                    |> TextField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "input" ]
                    |> Query.has
                        [ Selector.attribute (Attrs.disabled True)
                        ]
        , Test.test "input can have a max length" <|
            \() ->
                TextField.config
                    { id = "text-id"
                    , label = "My example text field"
                    }
                    |> TextField.setMaxLength (Just 100)
                    |> TextField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "input" ]
                    |> Query.has
                        [ Selector.attribute (Attrs.maxlength 100)
                        ]
        , Test.test "input can fire input event" <|
            \() ->
                TextField.config
                    { id = "text-id"
                    , label = "My example text field"
                    }
                    |> TextField.setOnInput Just
                    |> TextField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "input" ]
                    |> Event.simulate (Event.input "example input")
                    |> Event.expect (Just "example input")
        , Test.test "input can fire blur event" <|
            \() ->
                TextField.config
                    { id = "text-id"
                    , label = "My example text field"
                    }
                    |> TextField.setOnBlur 123
                    |> TextField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "input" ]
                    |> Event.simulate Event.blur
                    |> Event.expect 123
        ]
