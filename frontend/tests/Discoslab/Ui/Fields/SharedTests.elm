module Discoslab.Ui.Fields.SharedTests exposing (tests)

import Discoslab.Ui.Fields.Shared as SharedField
import Html exposing (Html)
import Html.Attributes as Attrs
import Test exposing (Test)
import Test.Html.Query as Query
import Test.Html.Selector as Selector


tests : (SharedField.Config -> Html msg) -> Test
tests view =
    Test.concat
        [ Test.test "has the correct label" <|
            \() ->
                view
                    { id = "example-id"
                    , label = "My Neat Example Label"
                    , isRequired = False
                    , isDisabled = False
                    , error = Nothing
                    , help = Nothing
                    }
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "label" ]
                    |> Query.has
                        [ Selector.class "field__label"
                        , Selector.attribute (Attrs.for "example-id")
                        , Selector.exactText "My Neat Example Label"
                        ]
        , Test.test "can be required" <|
            \() ->
                view
                    { id = "example-id"
                    , label = "My Neat Example Label"
                    , isRequired = True
                    , isDisabled = False
                    , error = Nothing
                    , help = Nothing
                    }
                    |> Query.fromHtml
                    |> Query.has
                        [ Selector.class "field"
                        , Selector.class "field--required"
                        ]
        , Test.test "can be disabled" <|
            \() ->
                view
                    { id = "example-id"
                    , label = "My Neat Example Label"
                    , isRequired = False
                    , isDisabled = True
                    , error = Nothing
                    , help = Nothing
                    }
                    |> Query.fromHtml
                    |> Query.has
                        [ Selector.class "field"
                        , Selector.class "field--disabled"
                        ]
        , Test.test "can have an error" <|
            \() ->
                view
                    { id = "example-id"
                    , label = "My Neat Example Label"
                    , isRequired = False
                    , isDisabled = False
                    , error = Just "This is an example error."
                    , help = Nothing
                    }
                    |> Query.fromHtml
                    |> Query.find [ Selector.class "field__error" ]
                    |> Query.has
                        [ Selector.tag "p"
                        , Selector.exactText "This is an example error."
                        ]
        , Test.test "can have help text" <|
            \() ->
                view
                    { id = "example-id"
                    , label = "My Neat Example Label"
                    , isRequired = False
                    , isDisabled = False
                    , error = Nothing
                    , help = Just "Example help text"
                    }
                    |> Query.fromHtml
                    |> Query.find [ Selector.class "field__help" ]
                    |> Query.has
                        [ Selector.tag "p"
                        , Selector.exactText "Example help text"
                        ]
        ]
