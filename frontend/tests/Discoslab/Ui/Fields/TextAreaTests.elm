module Discoslab.Ui.Fields.TextAreaTests exposing (tests)

import Discoslab.Ui.Fields.SharedTests as SharedTests
import Discoslab.Ui.Fields.TextArea as TextAreaField
import Html.Attributes as Attrs
import Test exposing (Test)
import Test.Html.Event as Event
import Test.Html.Query as Query
import Test.Html.Selector as Selector


tests : Test
tests =
    Test.describe "Discoslab.Ui.Fields.TextArea"
        [ SharedTests.tests <|
            \config ->
                TextAreaField.config
                    { id = config.id
                    , label = config.label
                    }
                    |> TextAreaField.setIsRequired config.isRequired
                    |> TextAreaField.setIsDisabled config.isDisabled
                    |> TextAreaField.setHelp config.help
                    |> TextAreaField.setError config.error
                    |> TextAreaField.view
        , Test.test "textarea has the correct id, classes, and type" <|
            \() ->
                TextAreaField.config
                    { id = "textarea-id"
                    , label = "My example textarea field"
                    }
                    |> TextAreaField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "textarea" ]
                    |> Query.has
                        [ Selector.id "textarea-id"
                        , Selector.class "field__input"
                        , Selector.class "field__input--textarea"
                        ]
        , Test.test "textarea can have a value" <|
            \() ->
                TextAreaField.config
                    { id = "textarea-id"
                    , label = "My example textarea field"
                    }
                    |> TextAreaField.setValue "some example value"
                    |> TextAreaField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "textarea" ]
                    |> Query.has
                        [ Selector.attribute (Attrs.value "some example value")
                        ]
        , Test.test "textarea can be disabled" <|
            \() ->
                TextAreaField.config
                    { id = "textarea-id"
                    , label = "My example textarea field"
                    }
                    |> TextAreaField.setIsDisabled True
                    |> TextAreaField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "textarea" ]
                    |> Query.has
                        [ Selector.attribute (Attrs.disabled True)
                        ]
        , Test.test "textarea can have a max length" <|
            \() ->
                TextAreaField.config
                    { id = "textarea-id"
                    , label = "My example textarea field"
                    }
                    |> TextAreaField.setMaxLength (Just 100)
                    |> TextAreaField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "textarea" ]
                    |> Query.has
                        [ Selector.attribute (Attrs.maxlength 100)
                        ]
        , Test.test "textarea can fire input event" <|
            \() ->
                TextAreaField.config
                    { id = "textarea-id"
                    , label = "My example textarea field"
                    }
                    |> TextAreaField.setOnInput Just
                    |> TextAreaField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "textarea" ]
                    |> Event.simulate (Event.input "example input")
                    |> Event.expect (Just "example input")
        , Test.test "textarea can fire blur event" <|
            \() ->
                TextAreaField.config
                    { id = "textarea-id"
                    , label = "My example textarea field"
                    }
                    |> TextAreaField.setOnBlur 123
                    |> TextAreaField.view
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "textarea" ]
                    |> Event.simulate Event.blur
                    |> Event.expect 123
        ]
