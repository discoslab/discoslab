module Discoslab.Ui.ButtonTests exposing (tests)

import Discoslab.Ui.Button as Button
import Html.Attributes as Attrs
import Test exposing (Test)
import Test.Html.Event as Event
import Test.Html.Query as Query
import Test.Html.Selector as Selector


tests : Test
tests =
    Test.describe "Discoslab.Ui.Button"
        [ Test.test "contains both an id and label" <|
            \() ->
                Button.config
                    { id = "example-id"
                    , label = "Example Label"
                    }
                    |> Button.view
                    |> Query.fromHtml
                    |> Query.has
                        [ Selector.id "example-id"
                        , Selector.exactText "Example Label"
                        , Selector.tag "button"
                        ]
        , Test.test "can be clicked" <|
            \() ->
                Button.config
                    { id = "example-id"
                    , label = "Example Label"
                    }
                    |> Button.setOnClick 123
                    |> Button.view
                    |> Query.fromHtml
                    |> Event.simulate Event.click
                    |> Event.expect 123
        , Test.describe "can render all three types of buttons" <|
            let
                examples =
                    [ ( Button.Normal, "button" )
                    , ( Button.Submit, "submit" )
                    , ( Button.Reset, "reset" )
                    ]

                testExample ( input, expected ) =
                    Test.test expected <|
                        \() ->
                            Button.config
                                { id = "example-id"
                                , label = "Example Label"
                                }
                                |> Button.setType input
                                |> Button.view
                                |> Query.fromHtml
                                |> Query.has
                                    [ Selector.attribute (Attrs.type_ expected)
                                    ]
            in
            List.map testExample examples
        , Test.describe "can render all three types of relations" <|
            let
                examples =
                    [ ( "primary", Button.Primary, Just "button--primary" )
                    , ( "secondary", Button.Secondary, Nothing )
                    , ( "dangerous", Button.Dangerous, Just "button--dangerous" )
                    ]

                testExample ( name, input, expected ) =
                    Test.test name <|
                        \() ->
                            Button.config
                                { id = "example-id"
                                , label = "Example Label"
                                }
                                |> Button.setRelation input
                                |> Button.view
                                |> Query.fromHtml
                                |> Query.has
                                    [ case expected of
                                        Nothing ->
                                            Selector.all []

                                        Just className ->
                                            Selector.class className
                                    ]
            in
            List.map testExample examples
        , Test.test "can be disabled" <|
            \() ->
                Button.config
                    { id = "example-id"
                    , label = "Example Label"
                    }
                    |> Button.setIsDisabled True
                    |> Button.view
                    |> Query.fromHtml
                    |> Query.has
                        [ Selector.class "button--disabled"
                        , Selector.attribute (Attrs.disabled True)
                        ]
        , Test.test "can be busy" <|
            \() ->
                Button.config
                    { id = "example-id"
                    , label = "Example Label"
                    }
                    |> Button.setIsBusy True
                    |> Button.view
                    |> Query.fromHtml
                    |> Query.has
                        [ Selector.class "button--busy"
                        ]
        ]
