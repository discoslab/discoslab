module Discoslab.Ui.Text.HeadingTests exposing (tests)

import Discoslab.Ui.Text.Heading as Heading
import Test exposing (Test)
import Test.Html.Query as Query
import Test.Html.Selector as Selector


tests : Test
tests =
    Test.describe "Discoslab.Ui.Text.Heading"
        [ Test.describe "No sub header"
            [ Test.describe "can render all three levels" <|
                let
                    examples =
                        [ ( "Low", Heading.Low, ( "h3", "heading--low" ) )
                        , ( "Normal", Heading.Normal, ( "h2", "heading--normal" ) )
                        , ( "High", Heading.High, ( "h1", "heading--high" ) )
                        ]

                    testExample ( name, input, ( tag, class ) ) =
                        Test.test name <|
                            \() ->
                                Heading.config "Example Top-Level Header"
                                    |> Heading.setLevel input
                                    |> Heading.view
                                    |> Query.fromHtml
                                    |> Query.has
                                        [ Selector.tag tag
                                        , Selector.class class
                                        , Selector.exactText "Example Top-Level Header"
                                        ]
                in
                List.map testExample examples
            , Test.describe "can render both alignments" <|
                let
                    examples =
                        [ ( "Left", Heading.Left, "heading--left" )
                        , ( "Center", Heading.Center, "heading--center" )
                        ]

                    testExample ( name, input, class ) =
                        Test.test name <|
                            \() ->
                                Heading.config "Example Top-Level Header"
                                    |> Heading.setAlignment input
                                    |> Heading.view
                                    |> Query.fromHtml
                                    |> Query.has
                                        [ Selector.tag "h2"
                                        , Selector.class class
                                        , Selector.exactText "Example Top-Level Header"
                                        ]
                in
                List.map testExample examples
            ]
        , Test.describe "With sub header" <|
            [ Test.describe "can render all three levels" <|
                let
                    examples =
                        [ ( "Low", Heading.Low, ( "h3", "heading--low" ) )
                        , ( "Normal", Heading.Normal, ( "h2", "heading--normal" ) )
                        , ( "High", Heading.High, ( "h1", "heading--high" ) )
                        ]

                    testExample ( name, input, ( tag, class ) ) =
                        let
                            html =
                                Heading.config "Example Top-Level Header"
                                    |> Heading.setSubHeader "Example sub-level header"
                                    |> Heading.setLevel input
                                    |> Heading.view
                                    |> Query.fromHtml
                        in
                        Test.describe name
                            [ Test.test "Top-level header is correct" <|
                                \() ->
                                    html
                                        |> Query.find [ Selector.class "heading__main" ]
                                        |> Query.has
                                            [ Selector.tag tag
                                            , Selector.exactText "Example Top-Level Header"
                                            ]
                            , Test.test "Sub-level header is correct" <|
                                \() ->
                                    html
                                        |> Query.find [ Selector.class "heading__sub" ]
                                        |> Query.has
                                            [ Selector.tag "p"
                                            , Selector.exactText "Example sub-level header"
                                            ]
                            , Test.test "Outer div has the correct level" <|
                                \() ->
                                    html
                                        |> Query.has
                                            [ Selector.tag "div"
                                            , Selector.class class
                                            ]
                            ]
                in
                List.map testExample examples
            , Test.describe "can render both alignments" <|
                let
                    examples =
                        [ ( "Left", Heading.Left, "heading--left" )
                        , ( "Center", Heading.Center, "heading--center" )
                        ]

                    testExample ( name, input, class ) =
                        let
                            html =
                                Heading.config "Example Top-Level Header"
                                    |> Heading.setSubHeader "Example sub-level header"
                                    |> Heading.setAlignment input
                                    |> Heading.view
                                    |> Query.fromHtml
                        in
                        Test.describe name
                            [ Test.test "Top-level header is correct" <|
                                \() ->
                                    html
                                        |> Query.find [ Selector.class "heading__main" ]
                                        |> Query.has
                                            [ Selector.tag "h2"
                                            , Selector.exactText "Example Top-Level Header"
                                            ]
                            , Test.test "Sub-level header is correct" <|
                                \() ->
                                    html
                                        |> Query.find [ Selector.class "heading__sub" ]
                                        |> Query.has
                                            [ Selector.tag "p"
                                            , Selector.exactText "Example sub-level header"
                                            ]
                            , Test.test "Outer div has the correct level" <|
                                \() ->
                                    html
                                        |> Query.has
                                            [ Selector.tag "div"
                                            , Selector.class class
                                            ]
                            ]
                in
                List.map testExample examples
            ]
        ]
