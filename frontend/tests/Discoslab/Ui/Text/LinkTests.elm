module Discoslab.Ui.Text.LinkTests exposing (tests)

import Discoslab.Ui.Text.Link as Link
import Html.Attributes as Attrs
import Test exposing (Test)
import Test.Html.Query as Query
import Test.Html.Selector as Selector


tests : Test
tests =
    Test.describe "Discoslab.Ui.Text.Link"
        [ Test.test "default display value is the href" <|
            \() ->
                Link.config "https://example.com/fake"
                    |> Link.view
                    |> Query.fromHtml
                    |> Query.has
                        [ Selector.class "link"
                        , Selector.exactText "https://example.com/fake"
                        , Selector.attribute (Attrs.href "https://example.com/fake")
                        , Selector.tag "a"
                        ]
        , Test.test "allows you to set a display value" <|
            \() ->
                Link.config "https://example.com/fake"
                    |> Link.setDisplayText "example display text"
                    |> Link.view
                    |> Query.fromHtml
                    |> Query.has
                        [ Selector.class "link"
                        , Selector.exactText "example display text"
                        , Selector.attribute (Attrs.href "https://example.com/fake")
                        , Selector.tag "a"
                        ]
        ]
