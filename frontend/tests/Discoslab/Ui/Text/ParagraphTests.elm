module Discoslab.Ui.Text.ParagraphTests exposing (tests)

import Discoslab.Ui.Text.Paragraph as Paragraph
import Html
import Test exposing (Test)
import Test.Html.Query as Query
import Test.Html.Selector as Selector


tests : Test
tests =
    Test.describe "Discoslab.Ui.Text.Paragraph"
        [ Test.test "renders with the paragraph class" <|
            \() ->
                Paragraph.view [ Html.text "example paragraph" ]
                    |> Query.fromHtml
                    |> Query.has
                        [ Selector.class "paragraph"
                        , Selector.exactText "example paragraph"
                        , Selector.tag "p"
                        ]
        ]
