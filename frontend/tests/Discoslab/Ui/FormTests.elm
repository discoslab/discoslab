module Discoslab.Ui.FormTests exposing (tests)

import Discoslab.Ui.Form as Form
import Html
import Test exposing (Test)
import Test.Html.Event as Event
import Test.Html.Query as Query
import Test.Html.Selector as Selector


tests : Test
tests =
    Test.describe "Discoslab.Ui.Form"
        [ Test.test "renders with an id" <|
            \() ->
                Form.config "my-form-id"
                    |> Form.view []
                    |> Query.fromHtml
                    |> Query.has
                        [ Selector.tag "form"
                        , Selector.id "my-form-id"
                        ]
        , Test.test "renders childrens" <|
            \() ->
                Form.config "my-form-id"
                    |> Form.view [ Html.p [] [ Html.text "this is a child" ] ]
                    |> Query.fromHtml
                    |> Query.find [ Selector.tag "p" ]
                    |> Query.has
                        [ Selector.exactText "this is a child"
                        ]
        , Test.test "can be submitted" <|
            \() ->
                Form.config "my-form-id"
                    |> Form.setOnSubmit 123
                    |> Form.view []
                    |> Query.fromHtml
                    |> Event.simulate Event.submit
                    |> Event.expect 123
        , Test.test "prevents default when submitted" <|
            \() ->
                Form.config "my-form-id"
                    |> Form.setOnSubmit 123
                    |> Form.view []
                    |> Query.fromHtml
                    |> Event.simulate Event.submit
                    |> Event.expectPreventDefault
        ]
