module Discoslab.Url.BasePathTests exposing (tests)

import Discoslab.Url.BasePath as BasePath exposing (BasePath)
import Expect
import Json.Decode as Decode
import Test exposing (Test)
import Url
import Url.Builder as UrlBuilder
import Url.Parser as UrlParser exposing ((</>), Parser)


type Example
    = Home
    | Foo Int
    | Bar String Int


exampleParser : BasePath -> Parser (Example -> a) a
exampleParser base =
    BasePath.path base
        </> UrlParser.oneOf
                [ UrlParser.map Home UrlParser.top
                , UrlParser.map Foo (UrlParser.s "foo" </> UrlParser.int)
                , UrlParser.map Bar (UrlParser.s "bar" </> UrlParser.string </> UrlParser.int)
                ]


tests : Test
tests =
    Test.describe "Discoslab.Url.BasePath"
        [ Test.describe "ignores extra slashes" <|
            let
                examples =
                    [ ( "/api/", "/api" )
                    , ( "api", "/api" )
                    , ( "/api", "/api" )
                    , ( "api/", "/api" )
                    , ( "/api/contacts", "/api/contacts" )
                    , ( "/api/contacts/", "/api/contacts" )
                    , ( "api/contacts/", "/api/contacts" )
                    , ( "api/contacts", "/api/contacts" )
                    , ( "  /api/contacts  ", "/api/contacts" )
                    , ( "/api/contacts/  ", "/api/contacts" )
                    , ( "  api/contacts/", "/api/contacts" )
                    , ( "   api/contacts    ", "/api/contacts" )
                    ]

                testExample ( example, expected ) =
                    let
                        actual =
                            BasePath.absolute (BasePath.fromString example) [] []
                    in
                    Test.test ("\"" ++ example ++ "\"") (\() -> Expect.equal expected actual)
            in
            List.map testExample examples
        , Test.test "absolute can add additional paths" <|
            \() ->
                let
                    expected =
                        "/api/first/second"

                    actual =
                        BasePath.absolute (BasePath.fromString "api") [ "first", "second" ] []
                in
                Expect.equal expected actual
        , Test.test "absolute can add query params" <|
            \() ->
                let
                    expected =
                        "/api/example?foo=123&bar=hello"

                    actual =
                        BasePath.absolute (BasePath.fromString "api/example") [] [ UrlBuilder.int "foo" 123, UrlBuilder.string "bar" "hello" ]
                in
                Expect.equal expected actual
        , Test.test "absolute can add query params and paths" <|
            \() ->
                let
                    expected =
                        "/api/example/abc/efg?foo=123&bar=hello"

                    actual =
                        BasePath.absolute (BasePath.fromString "api/example") [ "abc", "efg" ] [ UrlBuilder.int "foo" 123, UrlBuilder.string "bar" "hello" ]
                in
                Expect.equal expected actual
        , Test.describe "parser works with an empty base path" <|
            let
                examples =
                    [ ( "https://example.com", Home )
                    , ( "https://example.com/foo/123", Foo 123 )
                    , ( "https://example.com/foo/500/", Foo 500 )
                    , ( "https://example.com/bar/abc/123", Bar "abc" 123 )
                    , ( "https://example.com/bar/efg/2/", Bar "efg" 2 )
                    ]

                parser =
                    exampleParser (BasePath.fromString "")

                parseExample example =
                    Url.fromString example
                        |> Maybe.andThen (UrlParser.parse parser)

                testExample ( example, expected ) =
                    Test.test example <| \() -> Expect.equal (Just expected) (parseExample example)
            in
            List.map testExample examples
        , Test.describe "parser works with a non-empty base path" <|
            let
                examples =
                    [ ( "https://example.com/staging/api", Home )
                    , ( "https://example.com/staging/api/foo/123", Foo 123 )
                    , ( "https://example.com/staging/api/foo/500/", Foo 500 )
                    , ( "https://example.com/staging/api/bar/abc/123", Bar "abc" 123 )
                    , ( "https://example.com/staging/api/bar/efg/2/", Bar "efg" 2 )
                    ]

                parser =
                    exampleParser (BasePath.fromString "staging/api")

                parseExample example =
                    Url.fromString example
                        |> Maybe.andThen (UrlParser.parse parser)

                testExample ( example, expected ) =
                    Test.test example <| \() -> Expect.equal (Just expected) (parseExample example)
            in
            List.map testExample examples
        , Test.describe "encode and decode round trip" <|
            let
                examples =
                    [ "/api", "/api/abc", "/foo/bar/baz" ]

                testExample example =
                    let
                        actual =
                            BasePath.fromString example
                                |> BasePath.encode
                                |> Decode.decodeValue BasePath.decode

                        expected =
                            Ok (BasePath.fromString example)
                    in
                    Test.test example (\() -> Expect.equal expected actual)
            in
            List.map testExample examples
        ]
