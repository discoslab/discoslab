module DiscoslabTests exposing (tests)

import Discoslab.Test.Program as Program
import ProgramTest
import Test exposing (Test)
import Test.Html.Selector as Selector


tests : Test
tests =
    Test.describe "Discoslab"
        [ Test.test "welcome header is present" <|
            \() ->
                Program.start
                    |> ProgramTest.expectViewHas [ Selector.text "Welcome to Disco's Lab" ]
        ]
