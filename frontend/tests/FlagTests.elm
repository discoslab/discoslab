module FlagTests exposing (tests)

import Dict
import Discoslab.Effect.Logging as Logging
import Discoslab.Url.BasePath as BasePath exposing (BasePath)
import Expect exposing (Expectation)
import Flags exposing (Flags)
import Json.Encode as Encode
import Test exposing (Test)


parseAnyBasePath : String -> (Flags -> BasePath) -> List Test
parseAnyBasePath key selector =
    let
        examples =
            [ ( "", BasePath.fromString "/" )
            , ( "/", BasePath.fromString "/" )
            , ( "/api", BasePath.fromString "/api" )
            , ( "/custom/path", BasePath.fromString "/custom/path" )
            , ( " /custom/path  ", BasePath.fromString "/custom/path" )
            , ( " /custom/path/  ", BasePath.fromString "/custom/path" )
            , ( "custom/path", BasePath.fromString "/custom/path" )
            ]

        testExample ( input, expected ) =
            let
                actual =
                    Encode.object
                        [ ( key, Encode.string input )
                        ]
                        |> Flags.decode
                        |> Tuple.first
                        |> selector
            in
            Test.test ("\"" ++ input ++ "\"") <| \() -> Expect.equal expected actual
    in
    List.map testExample examples


expectJust : Maybe a -> Expectation
expectJust maybe =
    case maybe of
        Just _ ->
            Expect.pass

        Nothing ->
            Expect.fail "Maybe should not have been Nothing"


tests : Test
tests =
    Test.describe "Flags"
        [ Test.describe "webapiBasePath"
            [ Test.test "defaults to /api" <|
                \() ->
                    let
                        ( actualFlags, errors ) =
                            Flags.decode Encode.null
                    in
                    Expect.all
                        [ \_ -> Expect.equal actualFlags.webapiBasePath (BasePath.fromString "/api")
                        , \_ ->
                            Dict.get "webapiBasePath" errors
                                |> expectJust
                        ]
                        ()
            , Test.describe "can parse any valid base path" <|
                parseAnyBasePath "webapiBasePath" .webapiBasePath
            ]
        , Test.describe "frontendBasePath"
            [ Test.test "defaults to /" <|
                \() ->
                    let
                        ( actualFlags, errors ) =
                            Flags.decode Encode.null
                    in
                    Expect.all
                        [ \_ -> Expect.equal actualFlags.frontendBasePath (BasePath.fromString "/")
                        , \_ ->
                            Dict.get "frontendBasePath" errors
                                |> expectJust
                        ]
                        ()
            , Test.describe "can parse any valid base path" <|
                parseAnyBasePath "frontendBasePath" .frontendBasePath
            ]
        , Test.describe "minSeverity"
            [ Test.test "defaults to debug" <|
                \() ->
                    let
                        ( actualFlags, errors ) =
                            Flags.decode Encode.null
                    in
                    Expect.all
                        [ \_ -> Expect.equal actualFlags.minSeverity Logging.Debug
                        , \_ ->
                            Dict.get "minSeverity" errors
                                |> expectJust
                        ]
                        ()
            , Test.describe "can parse any valid severity" <|
                let
                    examples =
                        [ ( "debug", Logging.Debug )
                        , ( "info", Logging.Info )
                        , ( "notice", Logging.Notice )
                        , ( "warning", Logging.Warning )
                        , ( "error", Logging.Error )
                        , ( "critical", Logging.Critical )
                        , ( "alert", Logging.Alert )
                        , ( "emergency", Logging.Emergency )
                        , ( "DEBUG", Logging.Debug )
                        , ( "INFO", Logging.Info )
                        , ( "NOTICE", Logging.Notice )
                        , ( "WARNING", Logging.Warning )
                        , ( "ERROR", Logging.Error )
                        , ( "CRITICAL", Logging.Critical )
                        , ( "ALERT", Logging.Alert )
                        , ( "EMERGENCY", Logging.Emergency )
                        , ( "  deBug ", Logging.Debug )
                        , ( "  inFo ", Logging.Info )
                        , ( "  noTice  ", Logging.Notice )
                        , ( "  waRning  ", Logging.Warning )
                        , ( "  erRor  ", Logging.Error )
                        , ( "  crItical ", Logging.Critical )
                        , ( "  alErt  ", Logging.Alert )
                        , ( "  eMeRgeNCy ", Logging.Emergency )
                        , ( "  DEbUG   ", Logging.Debug )
                        , ( "  INfO  ", Logging.Info )
                        , ( "  NOtICE  ", Logging.Notice )
                        , ( "  WArNING ", Logging.Warning )
                        , ( "  ERrOR  ", Logging.Error )
                        , ( "  CRiTICAL ", Logging.Critical )
                        , ( "  ALeRT ", Logging.Alert )
                        , ( "  EMERGENCY", Logging.Emergency )
                        ]

                    testExample ( input, expected ) =
                        let
                            actual =
                                Encode.object
                                    [ ( "minSeverity", Encode.string input )
                                    ]
                                    |> Flags.decode
                                    |> Tuple.first
                                    |> .minSeverity
                        in
                        Test.test ("\"" ++ input ++ "\"") <| \() -> Expect.equal expected actual
                in
                List.map testExample examples
            ]
        ]
