-- | Functions for logging exceptions.
module Discoslab.Logging.Exceptions
  ( logException
  , logException_
  , withExceptionLogging
  )
where

import Control.Exception (SomeException)
import Control.Exception.Annotated.UnliftIO (AnnotatedException (..), Annotation (..), annotatedExceptionCallStack)
import Control.Exception.Annotated.UnliftIO qualified as AnnotatedException
import Control.Exception.Safe (isAsyncException)
import Control.Monad (unless)
import Control.Monad.IO.Unlift (MonadUnliftIO)
import Data.Maybe (isNothing)
import Data.Typeable (cast)
import GHC.Exception (CallStack)
import GHC.Stack (HasCallStack, prettyCallStack)
import Katip qualified


-- | Log an exception with its 'CallStack' and 'Annotation's.
logException
  :: ( Show e
     , Katip.KatipContext m
     , HasCallStack
     )
  => Katip.Severity
  -> AnnotatedException e
  -> Katip.LogStr
  -> m ()
logException severity exception message = do
  let callStackPayload =
        foldMap (Katip.sl "callStack" . prettyCallStack) $
          annotatedExceptionCallStack exception

      annotationsPayload =
        let isNotCallStack (Annotation annotation) =
              isNothing $ cast @_ @CallStack annotation
            annotations =
              filter isNotCallStack exception.annotations
         in if null annotations
              then mempty
              else Katip.sl "annotations" (fmap show annotations)

      exceptionPayload =
        Katip.sl "exception" (show exception.exception)

      contextPayload =
        mconcat
          [ callStackPayload
          , annotationsPayload
          , exceptionPayload
          ]

  Katip.katipAddContext contextPayload $ do
    Katip.logLocM severity message


-- | Log an exception with its 'CallStack' and 'Annotation's, with a default message.
logException_
  :: ( Show e
     , Katip.KatipContext m
     , HasCallStack
     )
  => Katip.Severity
  -> AnnotatedException e
  -> m ()
logException_ severity exception =
  logException severity exception "Exception thrown!"


-- | Wraps an action and ensures that any unhandled exception gets logged.
withExceptionLogging
  :: ( Katip.KatipContext m
     , MonadUnliftIO m
     , HasCallStack
     )
  => Katip.Severity
  -> m a
  -> m a
withExceptionLogging severity action = do
  result <- AnnotatedException.tryAnnotated @SomeException action

  case result of
    Right a ->
      pure a
    Left e -> do
      unless (isAsyncException e) $
        logException_ severity e

      AnnotatedException.throw e
