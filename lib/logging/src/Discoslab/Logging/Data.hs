-- | Type and functions for keeping track of the required data for logging.
module Discoslab.Logging.Data
  ( LoggingData (..)
  , noLogs
  , withStdout
  , runKatip
  , liftKatip
  , liftKatipIO
  )
where

import Discoslab.Logging.Color qualified as Color
import Discoslab.Logging.Config (LoggingConfig)
import Discoslab.Logging.Config qualified as LoggingConfig
import Discoslab.Logging.Format qualified as Format
import Discoslab.Logging.Metadata (Metadata)
import Discoslab.Logging.Metadata qualified as Metadata
import Discoslab.Logging.Metadata.Environment qualified as Environment
import Discoslab.Logging.MinSeverity qualified as MinSeverity

import Control.Exception (bracket)
import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad.IO.Unlift (MonadUnliftIO (..))
import Control.Monad.Reader
import Data.Has (Has (..))
import GHC.Stack (HasCallStack)
import Katip qualified
import System.IO (stdout)


-- | Data we need to do logging.
data LoggingData = LoggingData
  { env :: Katip.LogEnv
  , contexts :: Katip.LogContexts
  , namespace :: Katip.Namespace
  , metadata :: Metadata
  }


-- | Root namespace for this application. All logs will start with this namespace.
rootNamespace :: Katip.Namespace
rootNamespace =
  Katip.Namespace ["discoslab"]


-- | 'LoggingData' that doesn't record any logs.
-- This is mostly useful for running automated tests.
noLogs :: MonadIO m => Metadata -> m LoggingData
noLogs metadata = do
  env <- liftIO $ Katip.initLogEnv rootNamespace "nologs"
  pure $
    LoggingData
      { env
      , contexts = mempty
      , namespace = Katip.Namespace []
      , metadata = metadata
      }


-- | Make the log env and attach a 'stdout' scribe to it. It's important
-- that the caller takes care to clean this up appropriately, which
-- is why you should call 'withStdout' instead.
makeEnv :: HasCallStack => Katip.Namespace -> LoggingConfig -> IO Katip.LogEnv
makeEnv namespace config =
  checkpointCallStack $ do
    let permitFunc = MinSeverity.toPermitFunc config.minSeverity
        colorStrategy = Color.toColorStrategy config.color

    env <-
      Katip.initLogEnv (rootNamespace <> namespace) $
        Environment.toKatipEnvironment config.metadata.environment

    scribe <-
      Katip.mkHandleScribeWithFormatter
        (Format.toItemFormatter config.format)
        colorStrategy
        stdout
        permitFunc
        maxBound

    Katip.registerScribe "stdout" scribe Katip.defaultScribeSettings env


-- | Create 'LoggingData' that writes to 'stdout'. Uses a callback to ensure all
-- of the logs are flushed before exiting the program, even in the event of an
-- unhandled exception.
withStdout :: (HasCallStack, MonadUnliftIO m) => Katip.Namespace -> LoggingConfig -> (LoggingData -> m a) -> m a
withStdout namespace config use =
  checkpointCallStack $ withRunInIO $ \runInIO ->
    bracket (makeEnv namespace config) Katip.closeScribes $ \env ->
      runInIO . use $
        LoggingData
          { env
          , namespace = Katip.Namespace []
          , contexts = Katip.liftPayload $ Katip.sl "metadata" config.metadata
          , metadata = config.metadata
          }


-- | Run a 'Katip.KatipContextT' using 'LoggingData'.
runKatip :: LoggingData -> Katip.KatipContextT m a -> m a
runKatip loggingData =
  Katip.runKatipContextT loggingData.env loggingData.contexts loggingData.namespace


liftKatip :: (MonadReader r m, Has LoggingData r) => Katip.KatipContextT m a -> m a
liftKatip action = do
  loggingData <- asks getter
  runKatip loggingData action


liftKatipIO :: (MonadIO m, MonadReader r m, Has LoggingData r) => Katip.KatipContextT IO a -> m a
liftKatipIO action = do
  loggingData <- asks getter
  liftIO $ runKatip loggingData action
