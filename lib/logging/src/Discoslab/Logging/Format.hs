-- | Types and function for adjusting the format settings for logs.
module Discoslab.Logging.Format
  ( LogFormat (..)
  , def
  , toItemFormatter
  , toText
  , fromText
  , var
  )
where

import Discoslab.Common.BoundedEnum qualified as BoundedEnum
import Discoslab.Common.Environment (parseString)

import Data.Text (Text)
import Data.Text qualified as Text
import Env qualified
import Katip qualified


-- | Format to write the logs in.
data LogFormat
  = -- | Uses 'Katip.jsonFormat'. Recommended for production.
    JsonLogFormat
  | -- | Uses 'Katip.bracketFormat'. Recommended for local development.
    BracketLogFormat
  deriving (Show, Eq, Enum, Bounded)


-- | Defaults to 'JsonLogFormat'.
def :: LogFormat
def =
  JsonLogFormat


--- | Convert the 'LogFormat' to a 'Katip.ItemFormatter'.
toItemFormatter :: Katip.LogItem a => LogFormat -> Katip.ItemFormatter a
toItemFormatter = \case
  JsonLogFormat -> Katip.jsonFormat
  BracketLogFormat -> Katip.bracketFormat


-- | Convert the 'LogFormat' to 'Text'.
toText :: LogFormat -> Text
toText = \case
  JsonLogFormat -> "json"
  BracketLogFormat -> "bracket"


-- | Convert from 'Text' to 'LogFormat'.
-- Valid values are '"json"' or '"bracket"'.
fromText :: Text -> Maybe LogFormat
fromText =
  BoundedEnum.fromText toText


-- | Parse 'LogFormat' from an environment variable.
var :: Env.Parser Env.Error LogFormat
var =
  Env.var (parseString fromText) "FORMAT" . mconcat $
    [ Env.help "Format to log in. Valid values are json or bracket."
    , Env.def def
    , Env.helpDef (Text.unpack . toText)
    ]
