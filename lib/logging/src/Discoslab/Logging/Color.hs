-- | Types and function for adjusting the color settings for logs.
module Discoslab.Logging.Color
  ( Color (..)
  , def
  , toColorStrategy
  , fromBool
  , var
  )
where

import Discoslab.Common.Environment (bool)

import Env qualified
import Katip qualified


-- | Indicates that the logs should use color or not.
-- If you're doing local development, it helps to see warnings and higher
-- highlighted in your terminal. However, for production you'd want to
-- not use any color so that it's easier to parse the JSON logs later.
data Color
  = -- | Do not use any color, recommended for production.
    NoColor
  | -- | Do use color, recommended for local development.
    YesColor
  deriving (Show, Eq, Bounded, Enum)


-- | Default value for 'Color'. Defaults to 'NoColor'.
def :: Color
def =
  NoColor


-- | Convert a 'Color' to a 'Katip.ColorStrategy'.
toColorStrategy :: Color -> Katip.ColorStrategy
toColorStrategy =
  Katip.ColorLog . \case
    NoColor -> False
    YesColor -> True


-- | Convert a 'Bool' to 'Color'.
-- 'True' translates to 'YesColor', and 'False' to 'NoColor'.
fromBool :: Bool -> Color
fromBool useColor
  | useColor = YesColor
  | otherwise = NoColor


-- | Parse 'Color' from an environment variable.
var :: Env.Parser Env.Error Color
var =
  Env.var (fmap fromBool . bool) "USE_COLOR" . mconcat $
    [ Env.help "Use color when logging warnings or higher. Valid values are true or false."
    , Env.def def
    , Env.helpDef $ \case
        YesColor -> "true"
        NoColor -> "false"
    ]
