-- | Metadata describing things about this instance.
module Discoslab.Logging.Metadata
  ( Metadata (..)
  , def
  , vars
  )
where

import Discoslab.Logging.Metadata.Environment (Environment)
import Discoslab.Logging.Metadata.Environment qualified as Environment
import Discoslab.Logging.Metadata.InstanceId (InstanceId)
import Discoslab.Logging.Metadata.StartedAt (StartedAt)
import Discoslab.Logging.Metadata.Version (Version)
import Discoslab.Logging.Metadata.Version qualified as Version

import Data.Aeson qualified as Aeson
import Env qualified
import GHC.Generics (Generic)


-- | Metadata describing things about this instance.
data Metadata = Metadata
  { environment :: Environment
  , version :: Version
  , instanceId :: InstanceId
  , startedAt :: StartedAt
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON Metadata


-- | Default 'Metadata'. Calls @def@ from the respective modules.
def :: InstanceId -> StartedAt -> Metadata
def instanceId startedAt =
  Metadata
    { instanceId
    , startedAt
    , environment = Environment.def
    , version = Version.def
    }


vars :: Env.Parser Env.Error (InstanceId -> StartedAt -> Metadata)
vars =
  Metadata
    <$> Environment.var
    <*> Version.var
