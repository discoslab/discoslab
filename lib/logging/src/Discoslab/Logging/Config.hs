-- | Type and functions for configuring the logging.
module Discoslab.Logging.Config
  ( LoggingConfig (..)
  , def
  , vars
  )
where

import Discoslab.Logging.Color (Color)
import Discoslab.Logging.Color qualified as Color
import Discoslab.Logging.Format (LogFormat)
import Discoslab.Logging.Format qualified as LogFormat
import Discoslab.Logging.Metadata (Metadata)
import Discoslab.Logging.Metadata qualified as Metadata
import Discoslab.Logging.Metadata.InstanceId (InstanceId)
import Discoslab.Logging.Metadata.StartedAt (StartedAt)
import Discoslab.Logging.MinSeverity (MinSeverity)
import Discoslab.Logging.MinSeverity qualified as MinSeverity

import Env qualified


-- | Configuration for setting up logging.
data LoggingConfig = LoggingConfig
  { minSeverity :: MinSeverity
  , format :: LogFormat
  , color :: Color
  , metadata :: Metadata
  }
  deriving (Show, Eq)


-- | Default 'LoggingConfig'. Uses the @def@ function from the respective modules.
def :: InstanceId -> StartedAt -> LoggingConfig
def instanceId startedAt =
  LoggingConfig
    { minSeverity = MinSeverity.def
    , format = LogFormat.def
    , color = Color.def
    , metadata = Metadata.def instanceId startedAt
    }


-- | Parse the 'LoggingConfig' from environment variables.
vars :: Env.Parser Env.Error (InstanceId -> StartedAt -> LoggingConfig)
vars = do
  let prefix = Env.prefixed "LOGGING_"

  minSeverity <- prefix MinSeverity.var
  format <- prefix LogFormat.var
  color <- prefix Color.var
  metadata <- Metadata.vars

  pure $ \instanceId startedAt ->
    LoggingConfig
      { metadata = metadata instanceId startedAt
      , ..
      }
