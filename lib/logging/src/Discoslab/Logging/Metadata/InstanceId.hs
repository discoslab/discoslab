-- | Type and functions for identifying the instance.
module Discoslab.Logging.Metadata.InstanceId
  ( InstanceId
  , nextRandom
  , toUUID
  )
where

import Control.Monad.IO.Class (MonadIO (..))
import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.UUID (UUID)
import Data.UUID.V4 qualified as UUID


-- | Unique ID for this instance.
-- This should be new and unique every time the pogram starts.
newtype InstanceId = InstanceId UUID
  deriving (Show, Eq)


-- | Get the next random 'InstanceId'. Notice that we don't expose
-- the constructor of 'InstanceId', which means this is the only way
-- to make a new 'InstanceId'.
nextRandom :: MonadIO m => m InstanceId
nextRandom =
  liftIO $ fmap InstanceId UUID.nextRandom


-- | Convert the 'InstanceId' to a 'UUID'.
toUUID :: InstanceId -> UUID
toUUID =
  coerce


instance Aeson.ToJSON InstanceId where
  toJSON = Aeson.toJSON . toUUID
  toEncoding = Aeson.toEncoding . toUUID
