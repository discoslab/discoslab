module Main (main) where

import Discoslab.Logging.TestRunner (run)
import Spec (spec)


main :: IO ()
main = run spec
