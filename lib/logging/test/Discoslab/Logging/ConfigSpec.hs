module Discoslab.Logging.ConfigSpec
  ( spec
  ) where

import Discoslab.Logging.Color qualified as Color
import Discoslab.Logging.Config qualified as LoggingConfig
import Discoslab.Logging.Format qualified as Format
import Discoslab.Logging.Metadata (Metadata (Metadata))
import Discoslab.Logging.Metadata qualified as Metadata
import Discoslab.Logging.Metadata.Environment qualified as Environment
import Discoslab.Logging.Metadata.InstanceId qualified as InstanceId
import Discoslab.Logging.Metadata.StartedAt qualified as StartedAt
import Discoslab.Logging.Metadata.Version qualified as Version
import Discoslab.Logging.MinSeverity (MinSeverity (MinSeverity))
import Discoslab.Logging.MinSeverity qualified as MinSeverity

import Env qualified
import Katip qualified
import Test.Hspec (Spec, describe, it, runIO, shouldBe)


spec :: Spec
spec = do
  startedAt <- runIO StartedAt.now
  instanceId <- runIO InstanceId.nextRandom

  describe "def" $
    it "uses the correct values" $ do
      let config = LoggingConfig.def instanceId startedAt
      config.metadata `shouldBe` Metadata.def instanceId startedAt
      config.format `shouldBe` Format.def
      config.color `shouldBe` Color.def
      config.minSeverity `shouldBe` MinSeverity.def

  describe "vars" $ do
    let parse = Env.parsePure LoggingConfig.vars

    it "uses the correct default values" $ do
      Right config <- pure . fmap (\ctor -> ctor instanceId startedAt) $ parse []
      config.metadata `shouldBe` Metadata.def instanceId startedAt
      config.format `shouldBe` Format.def
      config.color `shouldBe` Color.def
      config.minSeverity `shouldBe` MinSeverity.def

    it "can parse all values" $ do
      let
        fakeEnv =
          [ ("LOGGING_MIN_SEVERITY", "critical")
          , ("LOGGING_FORMAT", "bracket")
          , ("LOGGING_USE_COLOR", "true")
          , ("ENVIRONMENT", "hspec")
          , ("VERSION", "my fake testing version")
          ]

      Just environment <- pure $ Environment.fromText "hspec"
      Just version <- pure $ Version.fromText "my fake testing version"

      let
        minSeverity = MinSeverity Katip.CriticalS

        format = Format.BracketLogFormat

        color = Color.YesColor

        metadata =
          Metadata
            { environment = environment
            , version = version
            , instanceId = instanceId
            , startedAt = startedAt
            }

      Right config <- pure . fmap (\ctor -> ctor instanceId startedAt) $ parse fakeEnv

      config.metadata `shouldBe` metadata
      config.format `shouldBe` format
      config.color `shouldBe` color
      config.minSeverity `shouldBe` minSeverity
