module Discoslab.Logging.FormatSpec
  ( spec
  ) where

import Discoslab.Logging.Format qualified as Format

import Data.Foldable (for_)
import Data.Text qualified as Text
import Data.Text.Lazy.Builder (Builder)
import Data.Time qualified as Time
import Data.Time.Calendar qualified as Calendar
import Env qualified
import Katip qualified
import Test.Hspec (Spec, describe, it, shouldBe, shouldNotBe)


itemFormatterBuilder :: Katip.ItemFormatter () -> Builder
itemFormatterBuilder itemFormatter =
  itemFormatter True Katip.V3 $
    Katip.Item
      { Katip._itemApp = "some app"
      , Katip._itemEnv = "some environment"
      , Katip._itemSeverity = Katip.ErrorS
      , Katip._itemThread = Katip.ThreadIdText "1234"
      , Katip._itemHost = "some host"
      , Katip._itemProcess = 20
      , Katip._itemPayload = ()
      , Katip._itemMessage = "some message"
      , Katip._itemTime = Time.UTCTime (Calendar.fromGregorian 2012 01 02) 0
      , Katip._itemNamespace = "some namespace"
      , Katip._itemLoc = Nothing
      }


spec :: Spec
spec = do
  let
    bracketFormatter = itemFormatterBuilder Katip.bracketFormat
    jsonFormatter = itemFormatterBuilder Katip.jsonFormat

    validJsonText =
      [ "json"
      , "JSON"
      , "JsOn"
      , "  json "
      , "  JSON "
      , "  JsOn "
      ]

    validBracketText =
      [ "bracket"
      , "BRACKET"
      , "BraCkEt"
      , "  bracket "
      , "  BRACKET "
      , "  BraCkEt "
      ]

    invalidText =
      [ ""
      , " "
      , "apm"
      , "elastic"
      , "splunk"
      , "sentry"
      ]

  it "can compare different item formatters" $ do
    bracketFormatter `shouldBe` bracketFormatter
    jsonFormatter `shouldBe` jsonFormatter
    bracketFormatter `shouldNotBe` jsonFormatter

  describe "def" $
    it "defaults to JsonLogFormat" $
      itemFormatterBuilder (Format.toItemFormatter Format.def) `shouldBe` jsonFormatter

  describe "toItemFormatter" $ do
    it "converts JsonLogFormat to Katip.jsonFormat" $
      itemFormatterBuilder (Format.toItemFormatter Format.JsonLogFormat) `shouldBe` jsonFormatter

    it "converts BracketLogFormat to Katip.bracketFormat" $
      itemFormatterBuilder (Format.toItemFormatter Format.BracketLogFormat) `shouldBe` bracketFormatter

  describe "toText" $ do
    it "converts JsonLogFormat to \"json\"" $
      Format.toText Format.JsonLogFormat `shouldBe` "json"

    it "converts BracketLogFormat to \"bracket\"" $
      Format.toText Format.BracketLogFormat `shouldBe` "bracket"

  describe "fromText" $ do
    describe "can parse json" $
      for_ validJsonText $ \text ->
        it (show text) $
          Format.fromText text `shouldBe` Just Format.JsonLogFormat

    describe "can parse bracket" $
      for_ validBracketText $ \text ->
        it (show text) $
          Format.fromText text `shouldBe` Just Format.BracketLogFormat

    describe "rejects invalid text" $
      for_ invalidText $ \text ->
        it (show text) $
          Format.fromText text `shouldBe` Nothing

  describe "var" $ do
    let
      varName =
        "FORMAT"

      parse =
        Env.parsePure Format.var

    it "defaults to the def value when not present" $
      parse [] `shouldBe` Right Format.def

    describe "can parse json" $
      for_ validJsonText $ \text ->
        it (show text) $
          parse [(varName, Text.unpack text)] `shouldBe` Right Format.JsonLogFormat

    describe "can parse bracket" $
      for_ validBracketText $ \text ->
        it (show text) $
          parse [(varName, Text.unpack text)] `shouldBe` Right Format.BracketLogFormat

    describe "rejects invalid values" $
      for_ invalidText $ \text ->
        it (show text) $
          let
            err
              | text == "" = Env.EmptyError
              | otherwise = Env.UnreadError (show text)
           in
            parse [(varName, Text.unpack text)] `shouldBe` Left [(varName, err)]
