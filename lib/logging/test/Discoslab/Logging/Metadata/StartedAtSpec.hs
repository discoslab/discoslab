module Discoslab.Logging.Metadata.StartedAtSpec
  ( spec
  ) where

import Discoslab.Logging.Metadata.StartedAt qualified as StartedAt

import Control.Concurrent (threadDelay)
import Control.Monad (replicateM)
import Data.Aeson qualified as Aeson
import Data.Foldable (for_)
import Data.List (nub, sort)
import Test.Hspec (Spec, describe, it, runIO, shouldBe)


spec :: Spec
spec = do
  someStartedAts <- runIO . replicateM 20 $ do
    threadDelay 1
    StartedAt.now

  describe "now" $
    it "should always produce later dates than the previous" $ do
      length (nub someStartedAts) `shouldBe` 20
      someStartedAts `shouldBe` sort someStartedAts

  describe "can be converted to json" $ do
    for_ someStartedAts $ \startedAt -> do
      let
        utcTime = StartedAt.toUTCTime startedAt

        expectedValue =
          Aeson.toJSON utcTime

        actualValue =
          Aeson.toJSON startedAt

        expectedEncoding =
          Aeson.toEncoding utcTime

        actualEncoding =
          Aeson.toEncoding startedAt
       in
        it (show startedAt) $ do
          actualValue `shouldBe` expectedValue
          actualEncoding `shouldBe` expectedEncoding
