module Discoslab.Logging.Metadata.EnvironmentSpec
  ( spec
  ) where

import Discoslab.Logging.Metadata.Environment (Environment)
import Discoslab.Logging.Metadata.Environment qualified as Environment

import Data.Aeson qualified as Aeson
import Data.Foldable (for_)
import Data.Text qualified as Text
import Env qualified
import Katip qualified
import Test.Hspec (Spec, describe, it, shouldBe)


spec :: Spec
spec = do
  let
    validText =
      [ "not set"
      , "dev"
      , "qa"
      , "prod"
      , "something really crazy long or whatever @#*(@*(%&@_!)) and symbols"
      ]

    invalidText =
      [ ""
      , "\t\n"
      , "    "
      ]

  describe "fromText" $ do
    describe "can parse any valid text" $
      for_ validText $ \text ->
        let
          actual =
            Environment.toText <$> Environment.fromText text
         in
          it (show text) $ actual `shouldBe` Just text

    describe "trims extra whitespace" $
      for_ validText $ \text ->
        let
          actual =
            Environment.toText <$> Environment.fromText ("   " <> text <> "   ")
         in
          it (show text) $ actual `shouldBe` Just text

    describe "rejects empty strings" $
      for_ invalidText $ \text ->
        it (show text) $ Environment.fromText text `shouldBe` Nothing

  describe "toKatipEnvironment" $
    it "can convert to Katip.Environment" $
      let
        actual =
          Environment.toKatipEnvironment <$> Environment.fromText "some sample environment"
       in
        actual `shouldBe` Just (Katip.Environment "some sample environment")

  describe "def" $
    it "defaults to \"local\"" $
      let
        actual = Environment.fromText "local"
       in
        Just Environment.def `shouldBe` actual

  describe "var" $ do
    let
      varName =
        "ENVIRONMENT"

      parse =
        Env.parsePure Environment.var

    it "defaults to the def value when not present" $
      parse [] `shouldBe` Right Environment.def

    describe "can parse any valid text" $
      for_ validText $ \text ->
        it (show text) $
          let actual =
                Environment.toText <$> parse [(varName, Text.unpack text)]
           in actual `shouldBe` Right text

    describe "trims extra whitespace" $
      for_ validText $ \text ->
        it (show text) $
          let actual =
                Environment.toText
                  <$> parse
                    [ (varName, Text.unpack ("  " <> text <> "  "))
                    ]
           in actual `shouldBe` Right text

    describe "rejects empty strings" $
      for_ invalidText $ \text ->
        it (show text) $
          let actual =
                Environment.toText <$> parse [(varName, Text.unpack text)]
              err
                | text == "" = Env.EmptyError
                | otherwise = Env.UnreadError (show text)
           in actual `shouldBe` Left [(varName, err)]

  describe "json" $ do
    describe "can be converted to json" $
      for_ validText $ \text ->
        let
          environment =
            Environment.fromText text

          expectedValue =
            Just $ Aeson.toJSON text

          actualValue =
            fmap Aeson.toJSON environment

          expectedEncoding =
            Just $ Aeson.toEncoding text

          actualEncoding =
            fmap Aeson.toEncoding environment
         in
          it (show text) $ do
            actualValue `shouldBe` expectedValue
            actualEncoding `shouldBe` expectedEncoding

    describe "can parse when valid" $
      for_ validText $ \text ->
        let
          json =
            Aeson.encode text

          parsedEnvironment =
            Environment.toText <$> Aeson.eitherDecode @Environment json
         in
          it (show text) $ parsedEnvironment `shouldBe` Right text

    describe "cannot parse when invalid" $
      for_ invalidText $ \text ->
        let
          json =
            Aeson.encode text

          parsedEnvironment =
            Aeson.eitherDecode @Environment json
         in
          it (show text) $ parsedEnvironment `shouldBe` Left "Error in $: Environment may not be an empty string."
