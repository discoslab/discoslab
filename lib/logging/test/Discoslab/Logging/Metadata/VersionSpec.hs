module Discoslab.Logging.Metadata.VersionSpec
  ( spec
  ) where

import Discoslab.Logging.Metadata.Version (Version)
import Discoslab.Logging.Metadata.Version qualified as Version

import Data.Aeson qualified as Aeson
import Data.Foldable (for_)
import Data.Text qualified as Text
import Env qualified
import Test.Hspec (Spec, describe, it, shouldBe)


spec :: Spec
spec = do
  let
    validText =
      [ "1.2.3"
      , "beta"
      , "something really crazy long or whatever @#*(@*(%&@_!)) and symbols"
      ]

    invalidText =
      [ ""
      , "\t\n"
      , "    "
      ]

  describe "fromText" $ do
    describe "can parse any valid text" $
      for_ validText $ \text ->
        let
          actual =
            Version.toText <$> Version.fromText text
         in
          it (show text) $ actual `shouldBe` Just text

    describe "trims extra whitespace" $
      for_ validText $ \text ->
        let
          actual =
            Version.toText <$> Version.fromText ("   " <> text <> "   ")
         in
          it (show text) $ actual `shouldBe` Just text

    describe "rejects empty strings" $
      for_ invalidText $ \text ->
        it (show text) $ Version.fromText text `shouldBe` Nothing

  describe "def" $
    it "defaults to \"not-versioned\"" $
      let
        actual = Version.fromText "not-versioned"
       in
        Just Version.def `shouldBe` actual

  describe "var" $ do
    let
      varName =
        "VERSION"

      parse =
        Env.parsePure Version.var

    it "defaults to the def value when not present" $
      parse [] `shouldBe` Right Version.def

    describe "can parse any valid text" $
      for_ validText $ \text ->
        it (show text) $
          let actual =
                Version.toText <$> parse [(varName, Text.unpack text)]
           in actual `shouldBe` Right text

    describe "trims extra whitespace" $
      for_ validText $ \text ->
        it (show text) $
          let actual =
                Version.toText
                  <$> parse
                    [ (varName, Text.unpack ("  " <> text <> "  "))
                    ]
           in actual `shouldBe` Right text

    describe "rejects empty strings" $
      for_ invalidText $ \text ->
        it (show text) $
          let actual =
                Version.toText <$> parse [(varName, Text.unpack text)]
              err
                | text == "" = Env.EmptyError
                | otherwise = Env.UnreadError (show text)
           in actual `shouldBe` Left [(varName, err)]

  describe "json" $ do
    describe "can be converted to json" $
      for_ validText $ \text ->
        let
          version =
            Version.fromText text

          expectedValue =
            Just $ Aeson.toJSON text

          actualValue =
            fmap Aeson.toJSON version

          expectedEncoding =
            Just $ Aeson.toEncoding text

          actualEncoding =
            fmap Aeson.toEncoding version
         in
          it (show text) $ do
            actualValue `shouldBe` expectedValue
            actualEncoding `shouldBe` expectedEncoding

    describe "can parse when valid" $
      for_ validText $ \text ->
        let
          json =
            Aeson.encode text

          parsedVersion =
            Version.toText <$> Aeson.eitherDecode @Version json
         in
          it (show text) $ parsedVersion `shouldBe` Right text

    describe "cannot parse when invalid" $
      for_ invalidText $ \text ->
        let
          json =
            Aeson.encode text

          parsedVersion =
            Aeson.eitherDecode @Version json
         in
          it (show text) $ parsedVersion `shouldBe` Left "Error in $: Version may not be an empty string."
