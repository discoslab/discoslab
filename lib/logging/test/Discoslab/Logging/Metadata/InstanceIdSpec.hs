module Discoslab.Logging.Metadata.InstanceIdSpec (spec) where

import Discoslab.Logging.Metadata.InstanceId qualified as InstanceId

import Control.Monad (replicateM)
import Data.Aeson qualified as Aeson
import Data.Foldable (for_)
import Data.List (nub)
import Test.Hspec (Spec, describe, it, runIO, shouldBe)


spec :: Spec
spec = do
  describe "nextRandom" $
    it "is always different" $ do
      someInstanceIds <- replicateM 20 InstanceId.nextRandom
      length (nub someInstanceIds) `shouldBe` 20

  describe "can be converted to json" $ do
    someInstanceIds <- runIO $ replicateM 20 InstanceId.nextRandom
    for_ someInstanceIds $ \instanceId -> do
      let
        uuid = InstanceId.toUUID instanceId

        expectedValue =
          Aeson.toJSON uuid

        actualValue =
          Aeson.toJSON instanceId

        expectedEncoding =
          Aeson.toEncoding uuid

        actualEncoding =
          Aeson.toEncoding instanceId
       in
        it (show instanceId) $ do
          actualValue `shouldBe` expectedValue
          actualEncoding `shouldBe` expectedEncoding
