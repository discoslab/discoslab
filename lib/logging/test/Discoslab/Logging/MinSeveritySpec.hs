module Discoslab.Logging.MinSeveritySpec
  ( spec
  ) where

import Discoslab.Logging.MinSeverity (MinSeverity (MinSeverity))
import Discoslab.Logging.MinSeverity qualified as MinSeverity

import Data.Coerce (coerce)
import Data.Foldable (for_)
import Data.Text qualified as Text
import Data.Time qualified as Time
import Data.Time.Calendar qualified as Calendar
import Env qualified
import Katip qualified
import Test.Hspec (Spec, describe, it, shouldBe)


makeItem :: MinSeverity -> Katip.Item ()
makeItem minSeverity =
  Katip.Item
    { Katip._itemApp = "some app"
    , Katip._itemEnv = "some environment"
    , Katip._itemSeverity = coerce minSeverity
    , Katip._itemThread = Katip.ThreadIdText "1234"
    , Katip._itemHost = "some host"
    , Katip._itemProcess = 20
    , Katip._itemPayload = ()
    , Katip._itemMessage = "some message"
    , Katip._itemTime = Time.UTCTime (Calendar.fromGregorian 2012 01 02) 0
    , Katip._itemNamespace = "some namespace"
    , Katip._itemLoc = Nothing
    }


spec :: Spec
spec = do
  let
    severities =
      [ (MinSeverity Katip.DebugS, "debug")
      , (MinSeverity Katip.InfoS, "info")
      , (MinSeverity Katip.NoticeS, "notice")
      , (MinSeverity Katip.WarningS, "warning")
      , (MinSeverity Katip.ErrorS, "error")
      , (MinSeverity Katip.AlertS, "alert")
      , (MinSeverity Katip.CriticalS, "critical")
      , (MinSeverity Katip.EmergencyS, "emergency")
      ]

    validSeverities =
      [ ("debug", MinSeverity Katip.DebugS)
      , ("DEBUG", MinSeverity Katip.DebugS)
      , ("DeBuG", MinSeverity Katip.DebugS)
      , ("  debug ", MinSeverity Katip.DebugS)
      , ("  DEBUG ", MinSeverity Katip.DebugS)
      , ("  DeBuG ", MinSeverity Katip.DebugS)
      , ("info", MinSeverity Katip.InfoS)
      , ("INFO", MinSeverity Katip.InfoS)
      , ("InFo", MinSeverity Katip.InfoS)
      , ("  info ", MinSeverity Katip.InfoS)
      , ("  INFO ", MinSeverity Katip.InfoS)
      , ("  InFo ", MinSeverity Katip.InfoS)
      , ("notice", MinSeverity Katip.NoticeS)
      , ("NOTICE", MinSeverity Katip.NoticeS)
      , ("nOTice", MinSeverity Katip.NoticeS)
      , (" notice ", MinSeverity Katip.NoticeS)
      , (" NOTICE ", MinSeverity Katip.NoticeS)
      , (" nOTice ", MinSeverity Katip.NoticeS)
      , ("warning", MinSeverity Katip.WarningS)
      , ("WARNING", MinSeverity Katip.WarningS)
      , ("WaRninG", MinSeverity Katip.WarningS)
      , ("  warning  ", MinSeverity Katip.WarningS)
      , ("  WARNING  ", MinSeverity Katip.WarningS)
      , ("  WaRninG  ", MinSeverity Katip.WarningS)
      , ("error", MinSeverity Katip.ErrorS)
      , ("ERROR", MinSeverity Katip.ErrorS)
      , ("ErRoR", MinSeverity Katip.ErrorS)
      , ("  error  ", MinSeverity Katip.ErrorS)
      , ("  ERROR  ", MinSeverity Katip.ErrorS)
      , ("  ErRoR  ", MinSeverity Katip.ErrorS)
      , ("alert", MinSeverity Katip.AlertS)
      , ("ALERT", MinSeverity Katip.AlertS)
      , ("ALeRt", MinSeverity Katip.AlertS)
      , ("  alert  ", MinSeverity Katip.AlertS)
      , ("  ALERT  ", MinSeverity Katip.AlertS)
      , ("  ALeRt  ", MinSeverity Katip.AlertS)
      , ("critical", MinSeverity Katip.CriticalS)
      , ("CRITICAL", MinSeverity Katip.CriticalS)
      , ("CrItIcaL", MinSeverity Katip.CriticalS)
      , ("  critical ", MinSeverity Katip.CriticalS)
      , ("  CRITICAL ", MinSeverity Katip.CriticalS)
      , ("  CrItIcaL ", MinSeverity Katip.CriticalS)
      , ("emergency", MinSeverity Katip.EmergencyS)
      , ("EMERGENCY", MinSeverity Katip.EmergencyS)
      , ("EmErgenCY", MinSeverity Katip.EmergencyS)
      , ("  emergency  ", MinSeverity Katip.EmergencyS)
      , ("  EMERGENCY  ", MinSeverity Katip.EmergencyS)
      , ("  EmErgenCY  ", MinSeverity Katip.EmergencyS)
      ]

    invalidSeverities =
      [ ""
      , " "
      , "danger"
      , "something else made up"
      ]

  describe "def" $
    it "defaults to minBound" $ do
      MinSeverity.def `shouldBe` minBound

  describe "toPermitFunc" $
    describe "works for all severities" $ do
      let
        allItems = fmap makeItem [minBound ..]

      for_ [minBound @MinSeverity ..] $ \minSeverity -> do
        let
          permitFunc = MinSeverity.toPermitFunc minSeverity

        it (show minSeverity) $
          for_ allItems $ \item -> do
            let
              expected =
                MinSeverity item._itemSeverity >= minSeverity

            actual <- permitFunc item

            actual `shouldBe` expected

  describe "toText" $
    describe "works for all severities" $
      for_ severities $ \(severity, text) ->
        it (show severity) $
          MinSeverity.toText severity `shouldBe` text

  describe "fromText" $ do
    describe "works for all valid severities" $
      for_ validSeverities $ \(input, expected) ->
        it (show input) $
          MinSeverity.fromText input `shouldBe` Just expected

    describe "rejects invalid severities" $
      for_ invalidSeverities $ \input ->
        it (show input) $
          MinSeverity.fromText input `shouldBe` Nothing

  describe "var" $ do
    let
      varName =
        "MIN_SEVERITY"

      parse =
        Env.parsePure MinSeverity.var

    it "defaults to the def value" $
      parse [] `shouldBe` Right MinSeverity.def

    describe "works for all valid severities" $
      for_ validSeverities $ \(input, expected) ->
        it (show input) $
          parse [(varName, Text.unpack input)] `shouldBe` Right expected

    describe "rejects invalid severities" $ do
      for_ invalidSeverities $ \text ->
        it (show text) $
          let
            err
              | text == "" = Env.EmptyError
              | otherwise = Env.UnreadError (show text)
           in
            parse [(varName, Text.unpack text)] `shouldBe` Left [(varName, err)]
