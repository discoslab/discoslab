module Discoslab.Logging.ColorSpec (spec) where

import Discoslab.Logging.Color (Color (..))
import Discoslab.Logging.Color qualified as Color

import Env qualified
import Katip qualified
import Test.Hspec (Spec, describe, it, shouldBe)


spec :: Spec
spec = do
  describe "def" $
    it "defaults to NoColor" $
      Color.def `shouldBe` NoColor

  describe "toColorStrategy" $ do
    it "NoColor gets converted to (ColorLog False)" $
      Color.toColorStrategy NoColor `shouldBe` Katip.ColorLog False

    it "YesColor gets converted to (ColorLog True)" $
      Color.toColorStrategy YesColor `shouldBe` Katip.ColorLog True

  describe "fromBool" $ do
    it "False gets converted to NoColor" $
      Color.fromBool False `shouldBe` NoColor

    it "True gets converted to NoColor" $
      Color.fromBool True `shouldBe` YesColor

  describe "var" $ do
    let
      varName =
        "USE_COLOR"

      parse =
        Env.parsePure Color.var

    it "defaults to the def value when not present" $
      parse [] `shouldBe` Right Color.def

    it "false is parsed as NoColor" $
      parse [(varName, "false")] `shouldBe` Right Color.NoColor

    it "true is parsed as YesColor" $
      parse [(varName, "true")] `shouldBe` Right Color.YesColor

    it "spits an error when value is unable to be parsed" $
      parse [(varName, "yes_color")] `shouldBe` Left [("USE_COLOR", Env.UnreadError "\"yes_color\"")]
