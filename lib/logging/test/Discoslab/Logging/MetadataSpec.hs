module Discoslab.Logging.MetadataSpec
  ( spec
  ) where

import Discoslab.Logging.Metadata qualified as Metadata
import Discoslab.Logging.Metadata.Environment qualified as Environment
import Discoslab.Logging.Metadata.InstanceId qualified as InstanceId
import Discoslab.Logging.Metadata.StartedAt qualified as StartedAt
import Discoslab.Logging.Metadata.Version qualified as Version

import Env
import Test.Hspec (Spec, it, runIO, shouldBe)


spec :: Spec
spec = do
  instanceId <- runIO InstanceId.nextRandom
  startedAt <- runIO StartedAt.now

  it "def returns the correct default values" $ do
    let metadata = Metadata.def instanceId startedAt

    metadata.version `shouldBe` Version.def
    metadata.environment `shouldBe` Environment.def
    metadata.instanceId `shouldBe` instanceId
    metadata.startedAt `shouldBe` startedAt

  it "can be parsed from environment variables" $ do
    Right makeMetadata <-
      pure . Env.parsePure Metadata.vars $
        [ ("ENVIRONMENT", "example environment")
        , ("VERSION", "example version")
        ]

    Just version <- pure $ Version.fromText "example version"
    Just environment <- pure $ Environment.fromText "example environment"

    let metadata = makeMetadata instanceId startedAt

    metadata.version `shouldBe` version
    metadata.environment `shouldBe` environment
    metadata.instanceId `shouldBe` instanceId
    metadata.startedAt `shouldBe` startedAt
