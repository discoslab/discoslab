module Main (main) where

import Discoslab.Common.TestRunner (run)
import Spec (spec)


main :: IO ()
main = run spec
