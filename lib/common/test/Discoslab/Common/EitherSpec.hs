module Discoslab.Common.EitherSpec
  ( spec
  ) where

import Discoslab.Common.Either (hush)

import Data.Text (Text)
import Test.Hspec (Spec, describe, it, shouldBe)


spec :: Spec
spec = do
  describe "hush" $ do
    it "converts to Just when the value is Right" $
      let
        example :: Either Text Integer
        example = Right 123
       in
        hush example `shouldBe` Just 123

    it "converts to Nothing when the value is Left" $
      let
        example :: Either Text Integer
        example = Left "Some example error message"
       in
        hush example `shouldBe` Nothing
