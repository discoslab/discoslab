module Discoslab.Common.JsonSpec
  ( spec
  ) where

import Discoslab.Common.Json (parseByteString, parseValue)

import Data.Aeson ((.=))
import Data.Aeson qualified as Aeson
import Data.ByteString qualified as ByteString
import Data.Foldable (for_)
import Data.Text (Text)
import Test.Hspec (Spec, SpecWith, describe, it, shouldBe)


canParseValues :: (Foldable t, Show a, Eq a, Aeson.ToJSON a, Aeson.FromJSON a) => t a -> SpecWith ()
canParseValues examples =
  for_ examples $ \example ->
    let jsonValue = Aeson.toJSON example
        parsedValue = parseValue Aeson.parseJSON jsonValue
     in it (show example) $ parsedValue `shouldBe` Right example


canParseByteStrings :: (Foldable t, Show a, Eq a, Aeson.ToJSON a, Aeson.FromJSON a) => t a -> SpecWith ()
canParseByteStrings examples =
  for_ examples $ \example ->
    let jsonValue = ByteString.toStrict $ Aeson.encode example
        parsedValue = parseByteString Aeson.parseJSON jsonValue
     in it (show example) $ parsedValue `shouldBe` Right example


spec :: Spec
spec = do
  let
    bools =
      [ True
      , False
      ]

    texts =
      [ "" :: Text
      , "dog"
      , "cat"
      , "bird"
      , "some longer sentence"
      ]

    numbers =
      [ 123 :: Double
      , 20.3
      , 400
      , -12.3
      , 0
      ]

    arrays =
      [ 123 :: Double
      , 20.3
      , 400
      , -12.3
      , 0
      ]

    objects =
      [ Aeson.object []
      , Aeson.object
          [ "name" .= ("Mochi" :: Text)
          , "age" .= (4 :: Integer)
          ]
      , Aeson.object
          [ "id" .= (23 :: Integer)
          , "list" .= [1 :: Integer, 2, 3, 4]
          , "object" .= Aeson.object ["bool" .= True]
          ]
      ]

  describe "parseValue" $ do
    describe "can parse bools" $ canParseValues bools
    describe "can parse texts" $ canParseValues texts
    describe "can parse numbers" $ canParseValues numbers
    describe "can parse arrays" $ canParseValues arrays
    describe "can parse objects" $ canParseValues objects
    it "returns an error when parsing fails" $
      parseValue (Aeson.parseJSON @Int) (Aeson.toJSON @Text "abc") `shouldBe` Left "Error in $: parsing Int failed, expected Number, but encountered String"

  describe "parseBytestring" $ do
    describe "can parse bools" $ canParseByteStrings bools
    describe "can parse texts" $ canParseByteStrings texts
    describe "can parse numbers" $ canParseByteStrings numbers
    describe "can parse arrays" $ canParseByteStrings arrays
    describe "can parse objects" $ canParseByteStrings objects
    it "returns an error when parsing fails" $
      parseByteString (Aeson.parseJSON @Int) "\"abc\"" `shouldBe` Left "Error in $: parsing Int failed, expected Number, but encountered String"
