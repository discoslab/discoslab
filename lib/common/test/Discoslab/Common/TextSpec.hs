module Discoslab.Common.TextSpec (spec) where

import Discoslab.Common.Text qualified as CommonText

import Data.Foldable (for_)
import Data.Text (Text)
import Test.Hspec (Spec, describe, it, shouldBe)


spec :: Spec
spec = do
  describe "normalize" . describe "ignores case, leading, and trailing spaces" $
    let
      examples :: [(Text, Text)]
      examples =
        [ ("abc", "abc")
        , ("ABC", "abc")
        , (" dOg  ", "dog")
        , (" doG", "  DOG ")
        ]
     in
      for_ examples $ \(left, right) ->
        it (show left <> " == " <> show right) $
          CommonText.normalize left `shouldBe` CommonText.normalize right
