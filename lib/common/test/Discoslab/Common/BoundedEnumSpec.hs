module Discoslab.Common.BoundedEnumSpec
  ( spec
  ) where

import Discoslab.Common.ExampleEnum
  ( ExampleEnum
  , exampleFromText
  , invalidExampleInputs
  , validExampleInputs
  )
import Discoslab.Common.Json (parseValue)

import Data.Aeson qualified as Aeson
import Data.Foldable (for_)
import Test.Hspec (Spec, describe, it, shouldBe)


spec :: Spec
spec = do
  describe "fromText" $ do
    describe "returns Just ExampleEnum for valid inputs" $
      for_ validExampleInputs $ \(input, expected) ->
        it (show input) $
          exampleFromText input `shouldBe` Just expected

    describe "returns Nothing for invalid inputs" $
      for_ invalidExampleInputs $ \input ->
        it (show input) $
          exampleFromText input `shouldBe` Nothing

  describe "parseJSON" $ do
    describe "returns Just ExampleEnum for valid inputs" $
      for_ validExampleInputs $ \(input, expected) ->
        it (show input) $
          parseValue Aeson.parseJSON (Aeson.toJSON input) `shouldBe` Right expected

    describe "returns Nothing for invalid inputs" $
      for_ invalidExampleInputs $ \input ->
        it (show input) $
          parseValue @ExampleEnum Aeson.parseJSON (Aeson.toJSON input) `shouldBe` Left ("Error in $: Invalid ExampleEnum detected: " <> input)
