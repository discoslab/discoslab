module Discoslab.Common.EnvironmentSpec
  ( spec
  ) where

import Discoslab.Common.Environment qualified as SharedEnv
import Discoslab.Common.ExampleEnum
  ( ExampleEnum
  , exampleFromText
  , invalidExampleInputs
  , validExampleInputs
  )

import Data.Foldable (for_)
import Data.Text qualified as Text
import Env qualified
import Test.Hspec (Spec, describe, it, shouldBe)


parseExampleEnum :: String -> Either Env.Error ExampleEnum
parseExampleEnum =
  SharedEnv.parseString exampleFromText


newtype LessThan10 = LessThan10 Int
  deriving (Show, Eq)


parseLessThan10 :: String -> Either Env.Error LessThan10
parseLessThan10 =
  SharedEnv.parseAuto $ \int ->
    if int < 10
      then Just $ LessThan10 int
      else Nothing


expectedError :: Bool -> String -> Env.Error
expectedError shouldShow = \case
  "" -> Env.EmptyError
  e ->
    Env.UnreadError $
      if shouldShow
        then show e
        else e


spec :: Spec
spec = do
  describe "parseString" $ do
    describe "returns the correct value for things we recognize" $
      for_ validExampleInputs $ \(input, expected) ->
        it (show input) $
          let actual = parseExampleEnum $ Text.unpack input
           in actual `shouldBe` Right expected

    describe "returns the correct error for things we do not recognize" $
      for_ invalidExampleInputs $ \input ->
        it (show input) $
          let actual = parseExampleEnum $ Text.unpack input
           in actual `shouldBe` Left (expectedError True $ Text.unpack input)

  describe "parseAuto" $ do
    let validInputs =
          [ (show i, LessThan10 i)
          | i <- [-2 .. 9]
          ]

        invalidInputs =
          fmap (show @Int) [10 .. 20]

    describe "returns the correct value for things we recognize" $
      for_ validInputs $ \(input, expected) ->
        it (show input) $
          let actual = parseLessThan10 input
           in actual `shouldBe` Right expected

    describe "returns the correct error for things we do not recognize" $
      for_ invalidInputs $ \input ->
        it (show input) $
          let actual = parseLessThan10 input
           in actual `shouldBe` Left (expectedError False input)

  describe "bool" $ do
    let validInputs =
          [ ("true", True)
          , ("t", True)
          , ("1", True)
          , ("yes", True)
          , ("y", True)
          , ("on", True)
          , ("false", False)
          , ("f", False)
          , ("0", False)
          , ("no", False)
          , ("n", False)
          , ("off", False)
          , (" trUE   ", True)
          , (" T  ", True)
          , ("1 ", True)
          , ("  yEs", True)
          , ("Y", True)
          , ("On", True)
          , ("  fALse  ", False)
          , (" f ", False)
          , (" 0 ", False)
          , ("nO   ", False)
          , (" n ", False)
          , ("  off", False)
          ]

        invalidInputs =
          [ "plzno"
          , "ohgodno"
          , "adasdasdas"
          , "$@#*(@#(*@#))"
          , "    "
          , ""
          , "yesyesyesyeysyseys"
          , "yeah"
          , "yep"
          , "nawh"
          , "nope"
          ]

    describe "returns the correct value for things we recognize" $
      for_ validInputs $ \(input, expected) ->
        it (show input) $
          let actual = SharedEnv.bool input
           in actual `shouldBe` Right expected

    describe "returns the correct error for things we do not recognize" $
      for_ invalidInputs $ \input ->
        it (show input) $
          let actual = SharedEnv.bool input
           in actual `shouldBe` Left (expectedError True input)
