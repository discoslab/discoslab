module Discoslab.Common.Json
  ( parseValue
  , parseByteString
  ) where

import Data.Aeson qualified as Aeson
import Data.Aeson.Types qualified as AesonTypes
import Data.Bifunctor (Bifunctor (first))
import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Text qualified as Text


parseValue :: (Aeson.Value -> AesonTypes.Parser a) -> Aeson.Value -> Either Text a
parseValue parser json =
  first Text.pack $ AesonTypes.parseEither parser json


parseByteString :: (Aeson.Value -> AesonTypes.Parser a) -> ByteString -> Either Text a
parseByteString parser json = do
  jsonValue <- first Text.pack $ Aeson.eitherDecodeStrict json
  parseValue parser jsonValue
