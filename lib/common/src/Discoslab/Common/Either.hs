module Discoslab.Common.Either
  ( hush
  ) where


hush :: Either e a -> Maybe a
hush =
  either (const Nothing) Just
