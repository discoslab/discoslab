module Discoslab.Common.Text
  ( normalize
  ) where

import Data.Text (Text)
import Data.Text qualified as Text


-- | Strip trailing/leading white space and convert to case fold so we
-- can do case-insensitive searches later.
normalize :: Text -> Text
normalize =
  Text.toCaseFold . Text.strip
