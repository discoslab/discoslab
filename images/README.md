# Disco's Lab Docker Images
Source files and registry for docker images used at Disco's lab.

## Images
- [haskell](./haskell): Tools for building applications written in Haskell.
