#!/bin/bash

set -ex

ghc --version
cabal --version
fourmolu --version
hlint --version
