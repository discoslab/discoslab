# haskell
Image based off of `debian:bullseye-slim`, designed for handling all things related to Haskell.
Including things such as compiling, testing, linting, and formatting of Haskell projects.

## Binaries added
  - `ghc`: Haskell compiler
  - `cabal`: Build tool for Haskell packages
  - `stack`: Another build tool for Haskell packages
  - `fourmolu`: Formatter for Haskell
  - `hlint`: Linter for Haskell 

## How to pull
`docker pull registry.gitlab.com/discoslab/discoslab/haskell:9.4.4`

## How to build
`docker build -t discoslab-haskell .`

To speed up builds you may use `--cache-from=` flag like so:

`docker build --cache-from registry.gitlab.com/discoslab/discoslab/haskell:9.2.7 --tag discoslab-haskell .`
  
## How tagging works
The images are tagged by `ghc` version.
Tags that end in branch names correspond to the `HEAD` of their respective branch.
Tags that end in commit SHAs correspond to that exact commit in this repo.

For example:
  - `registry.gitlab.com/discoslab/discoslab/haskell:9.2.7` is from the `main` branch and includes `ghc` `9.2.7`.
  - `registry.gitlab.com/discoslab/discoslab/haskell:9.2.7-refactor` is from the `refactor` branch and includes `ghc` `9.2.7`.
  - `registry.gitlab.com/discoslab/discoslab/haskell:9.2.7-0c7c236fc6403c3eda532cc9bf184c5d489cf0b8` is from the `0c7c236fc6403c3eda532cc9bf184c5d489cf0b8` commit and includes `ghc` `9.2.6`.
